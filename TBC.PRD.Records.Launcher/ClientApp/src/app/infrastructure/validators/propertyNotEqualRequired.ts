import { AbstractControl } from "@angular/forms";
import { isNilOrEmpty } from "../utils/is-or-not/isNilOrEmpty";
import { isNotNilOrEmpty } from "../utils/is-or-not/isNotNilOrEmpty";

export function propertyNotEqualRequired(
  getFormGroup: Function,
  property: string
) {
  return (formControl: AbstractControl) => {
    const formGroup = getFormGroup();

    if (!formGroup) {
      return null;
    }

    if (isNotNilOrEmpty(formControl.value) && formControl.value === property) {
      return { required: true };
    }

    return null;
  };
}
