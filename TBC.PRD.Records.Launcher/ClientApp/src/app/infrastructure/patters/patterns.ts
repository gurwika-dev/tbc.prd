export const Patterns = {
  latinOrGeorgian: "(^([a-zA-Z]+|[ა-ჰ]+)$)",
  mobile: "(^[0-9]{9}$|^[0-9]{12}$)",
  mail: "^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$",
  personalNumber: "^[0-9_-]{11}$"
};
