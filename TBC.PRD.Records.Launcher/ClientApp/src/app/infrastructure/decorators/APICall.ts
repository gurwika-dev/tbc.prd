interface Props {
  types: Array<string>;
  meta?: Object;
}

export interface ICallAPI {
  REQUEST: (
    payload?: any,
    onSuccess?: Function,
    onFailure?: Function,
    callback?: Function
  ) => {
    type: string;
    payload: any;
    onSuccess?: Function;
    onFailure?: Function;
    callback?: Function;
  };
  SUCCESS: (payload) => { type: string; payload: any };
  FAILURE: (err?: any) => { type: string; err: any };
}

export function APICall(props: Props): PropertyDecorator {
  return function(target: any, propertyKey: string) {
    const { types, meta } = props;
    const [requestType, successType, failureType] = types;

    Object.defineProperty(target, propertyKey, {
      value: {},
      enumerable: true,
      configurable: true
    });

    target[propertyKey].REQUEST = function(
      payload,
      onSuccess,
      onFailure,
      callback
    ) {
      return { type: requestType, payload, onSuccess, onFailure, callback };
    };

    target[propertyKey].SUCCESS = function(payload) {
      return { type: successType, payload };
    };

    target[propertyKey].FAILURE = function(payload) {
      return { type: failureType, payload };
    };
  };
}
