export function WithNumberInputs(fields: Array<string>): ClassDecorator {
  return function(constructor: any) {
    fields.forEach(field => {
      constructor.prototype[field] = null;
      Object.defineProperty(constructor.prototype, field, {
        get: function() {
          return this[`_${field}`];
        },
        set: function(value: number) {
          this[`_${field}`] = value ? Number(value) : null;
        },
        configurable: false,
        enumerable: true,
      });
    });
  };
}
