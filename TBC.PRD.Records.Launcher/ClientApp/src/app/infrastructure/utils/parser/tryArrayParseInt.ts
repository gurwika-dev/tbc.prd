import { defaultTo } from "ramda";
import { isNotNaN } from "../is-or-not/isNOtNan";

export const tryArrayParseInt = props => {
  props.forEach((element, index) => {
    const data = parseInt(element, NaN);
    if (isNotNaN(data)) {
      props[index] = data;
    }
  });

  return props;
};
export const tryParseInt = props => {
  const data = parseInt(defaultTo("")(props).replace(/,/g, ""), NaN);

  if (isNotNaN(data)) {
    return data;
  }

  return props;
};
