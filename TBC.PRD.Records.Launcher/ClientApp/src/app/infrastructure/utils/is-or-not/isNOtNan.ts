import { complement } from 'ramda';

export const isNotNaN = complement(isNaN);
