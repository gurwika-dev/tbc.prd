import { isNotNilOrEmpty } from './isNotNilOrEmpty';

export const isPositiveDescimal = number => {
  const value = Number(number.replace(/,/g, ''));

  return isNotNilOrEmpty(value) && value >= 1 && number.charAt(0) !== '0';
};
