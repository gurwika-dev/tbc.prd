export const isNumber = number => {
  const regex = /^([0-9])*$/;

  return number.match(regex) && number.charAt(0) !== '0';
};
