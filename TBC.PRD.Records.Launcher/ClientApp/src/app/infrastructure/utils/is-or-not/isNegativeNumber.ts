import { isNotNilOrEmpty } from './isNotNilOrEmpty';

export const isNegativeNumber = (number, minValue: number) => {
  const value = parseInt(number, NaN);

  return (
    (isNotNilOrEmpty(value) &&
      value >= minValue &&
      value === Number(number) &&
      !number.includes('.') &&
      (value !== 0 || number.length === 1)) ||
    number === '-'
  );
};
