import { complement } from 'ramda';
import { isNilOrEmpty } from './isNilOrEmpty';

export const isNotNilOrEmpty = complement(isNilOrEmpty);
