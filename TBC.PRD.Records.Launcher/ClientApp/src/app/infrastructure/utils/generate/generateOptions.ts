import {
  transduce,
  filter,
  map,
  compose,
  flip,
  append,
  keys,
  complement
} from "ramda";

const isNumeric = complement(isNaN);

const transducer = name =>
  compose(
    filter(isNumeric),
    map(key => ({
      translate: true,
      text: "enums." + name + "." + key,
      value: key
    }))
  );

export const generateOptions = props => {
  const enumName = keys(props)[0];
  return transduce(
    transducer(enumName),
    flip(append),
    [],
    keys(props[enumName])
  );
};
