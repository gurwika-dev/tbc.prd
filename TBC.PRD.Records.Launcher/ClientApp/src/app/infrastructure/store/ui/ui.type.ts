export enum NavigationActionType {
  SHOW = "[UI] Navigation SHOW",
  HIDE = "[UI] Navigation HIDE"
}
