export interface NavigationCondition {
  visible: boolean;
}
