import { Injectable } from "@angular/core";

import { NavigationActionType } from "./ui.type";

@Injectable()
export class UIAction {
  showNavigation() {
    return {
      type: NavigationActionType.SHOW
    };
  }

  hideNavigation() {
    return {
      type: NavigationActionType.HIDE
    };
  }
}
