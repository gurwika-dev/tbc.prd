import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { empty } from "rxjs";
import { mergeMap, take, map } from "rxjs/operators";

import { UIAction } from "./ui.action";
import { AppState } from "../app-state.model";

@Injectable()
export class UIEffect {
  constructor(
    private actions$: Actions,
    private action: UIAction,
    private store: Store<AppState>
  ) {}
}
