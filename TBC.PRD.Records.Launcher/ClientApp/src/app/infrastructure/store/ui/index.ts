import { UIAction } from "./ui.action";
import { UIEffect } from "./ui.effect";

export * from "./ui.action";

export const UI_PROVIDERS: any[] = [UIAction];
export const UI_EFFECTS: any[] = [UIEffect];
