import { NavigationCondition } from "./models/navigation-condition";

export interface UIState {
  navigation: NavigationCondition;
}

export const initialState: UIState = {
  navigation: {
    visible: false
  }
};
