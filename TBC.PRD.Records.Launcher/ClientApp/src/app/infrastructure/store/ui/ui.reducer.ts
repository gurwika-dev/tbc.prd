import { NavigationActionType } from "./ui.type";
import { AppStoreAction } from "../app-store.action";
import { mergeDeepRight } from "ramda";
import { UIState, initialState } from "./ui.model";

export function uiReducer(
  state: UIState = initialState,
  action: AppStoreAction
) {
  switch (action.type) {
    case NavigationActionType.SHOW:
      return mergeDeepRight(state, {
        navigation: {
          visible: true
        }
      });
    case NavigationActionType.HIDE:
      return mergeDeepRight(state, {
        navigation: {
          visible: false
        }
      });

    default:
      return state;
  }
}
