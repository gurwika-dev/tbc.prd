import { UIState } from "./ui/ui.model";

export interface AppState {
  ui: UIState;
}
