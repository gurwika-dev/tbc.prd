import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { StoreModule } from "@ngrx/store";
import { StoreDevtoolsModule } from "@ngrx/store-devtools";
import { StoreRouterConnectingModule } from "@ngrx/router-store";
import { EffectsModule } from "@ngrx/effects";
import { environment } from "src/environments/environment";
import { UI_EFFECTS, UI_PROVIDERS } from "./ui";
import { HttpClientModule } from "@angular/common/http";
import { appStoreReducers } from "./app-store.reducers";

const SDK_PROVIDERS: Array<any> = [UI_PROVIDERS];
const SDK_EFFECTS: any[] = [...UI_EFFECTS];

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,

    // Store
    StoreModule.forRoot(appStoreReducers, {
      runtimeChecks: {
        strictStateImmutability: true,
        strictActionImmutability: true
      }
    }),
    EffectsModule.forRoot(SDK_EFFECTS),

    // Redux
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: !environment.production
    }),

    // Connects RouterModule with StoreModule
    StoreRouterConnectingModule.forRoot()
  ],
  providers: [SDK_PROVIDERS],
  exports: [CommonModule, StoreModule, StoreRouterConnectingModule]
})
export class AppStoreModule {}
