import { ActionReducerMap } from "@ngrx/store";

import { uiReducer } from "./ui/ui.reducer";
import { AppState } from "./app-state.model";

export const appStoreReducers: ActionReducerMap<AppState> = {
  ui: uiReducer
};
