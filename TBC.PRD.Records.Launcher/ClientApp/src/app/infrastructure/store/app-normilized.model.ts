export interface AppNormilized<T> {
  entities: {
    [key: string]: T;
  };
  keys: string[];
  count?: number;
}
