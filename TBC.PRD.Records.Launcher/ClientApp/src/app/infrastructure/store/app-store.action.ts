import { Action } from "@ngrx/store";

export interface AppStoreAction extends Action {
  payload?: any;
  onSuccess?: Function;
  onFailure?: Function;
  callback?: Function;
}
