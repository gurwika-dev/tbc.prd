import { Directive, Input, Renderer, ElementRef, Output, EventEmitter, HostListener } from '@angular/core';
import { forEach } from 'ramda';

@Directive({
  selector: '[appUploadFile]',
})
export class UploadFileDirective {
  @Input()
  accept: string;

  @Output()
  file: EventEmitter<any> = new EventEmitter();

  @HostListener('change', ['$event'])
  attachFile(event) {
    const { files } = event.target;
    forEach(file => {
      this.file.emit(file);
    }, files);

    this._eref.nativeElement.value = '';
  }

  constructor(private _renderer: Renderer, private _eref: ElementRef) {
    this._renderer.setElementAttribute(this._eref.nativeElement, 'type', 'file');
    this._renderer.setElementAttribute(this._eref.nativeElement, 'accept', this.accept);
  }
}
