import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClickOutsideDirective } from './click-outside/click-outside.directive';
import { UploadFileDirective } from './upload-file/upload-file.directive';

const DIRECTIVES = [ClickOutsideDirective, UploadFileDirective];

@NgModule({
  imports: [CommonModule],
  declarations: DIRECTIVES,
  exports: DIRECTIVES,
})
export class DirectivesModule {}
