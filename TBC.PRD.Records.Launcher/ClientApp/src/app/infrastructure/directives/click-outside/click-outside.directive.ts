import {
  Directive,
  Output,
  EventEmitter,
  OnDestroy,
  ElementRef,
  Input,
  Renderer2
} from "@angular/core";
import { DeviceResolutionService } from "src/app/application/services/device-resolution.service";

@Directive({
  selector: "[appClickOutside]"
})
export class ClickOutsideDirective implements OnDestroy {
  @Output()
  public clickOutside = new EventEmitter<MouseEvent>();

  @Input()
  public outsideElementQuery: string;

  globalListenFunc: Function;

  constructor(
    private renderer: Renderer2,
    private deviceResolutionService: DeviceResolutionService,
    private ref: ElementRef
  ) {
    if (
      this.deviceResolutionService.isTablet ||
      this.deviceResolutionService.isMobile
    ) {
      this._initTouchOutside();
    } else {
      this._initClickOutside();
    }
  }

  _initTouchOutside() {
    this.globalListenFunc = this.renderer.listen(
      "window",
      "touchstart",
      $event => {
        let clickedInside = this.ref.nativeElement.contains($event.target);

        if (this.outsideElementQuery) {
          clickedInside =
            clickedInside ||
            document
              .querySelector(this.outsideElementQuery)
              .contains($event.target);
        }

        if (!clickedInside) {
          this.clickOutside.emit($event);
        }
      }
    );
  }

  _initClickOutside() {
    this.globalListenFunc = this.renderer.listen(
      "document",
      "click",
      $event => {
        let clickedInside = this.ref.nativeElement.contains($event.target);

        if (this.outsideElementQuery) {
          clickedInside =
            clickedInside ||
            document
              .querySelector(this.outsideElementQuery)
              .contains($event.target);
        }

        if (!clickedInside) {
          this.clickOutside.emit($event);
        }
      }
    );
  }

  ngOnDestroy() {
    this.globalListenFunc();
  }
}
