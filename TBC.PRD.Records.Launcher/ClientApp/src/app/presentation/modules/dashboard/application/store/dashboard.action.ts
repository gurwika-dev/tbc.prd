import { Injectable } from "@angular/core";
import {
  GetCities,
  GetPersons,
  GetPersonStatisticsById,
  GetPersonById,
  UpdatePerson,
  CreatePerson,
  DeletePerson,
  GetPersonRelations,
  CreatePersonRelation,
  DeletePersonRelation
} from "./dashboard.type";
import { ICallAPI, APICall } from "src/app/infrastructure/decorators/APICall";

@Injectable()
export class DashboardAction {
  @APICall({
    types: [GetCities.REQUEST, GetCities.SUCCESS, GetCities.FAILURE]
  })
  getCities: ICallAPI;

  @APICall({
    types: [GetPersons.REQUEST, GetPersons.SUCCESS, GetPersons.FAILURE]
  })
  getPersons: ICallAPI;

  @APICall({
    types: [GetPersonById.REQUEST, GetPersonById.SUCCESS, GetPersonById.FAILURE]
  })
  getPersonById: ICallAPI;

  @APICall({
    types: [
      GetPersonStatisticsById.REQUEST,
      GetPersonStatisticsById.SUCCESS,
      GetPersonStatisticsById.FAILURE
    ]
  })
  getPersonStatisticsById: ICallAPI;

  @APICall({
    types: [CreatePerson.REQUEST, CreatePerson.SUCCESS, CreatePerson.FAILURE]
  })
  createPerson: ICallAPI;

  @APICall({
    types: [UpdatePerson.REQUEST, UpdatePerson.SUCCESS, UpdatePerson.FAILURE]
  })
  updatePerson: ICallAPI;

  @APICall({
    types: [DeletePerson.REQUEST, DeletePerson.SUCCESS, DeletePerson.FAILURE]
  })
  deletePerson: ICallAPI;

  @APICall({
    types: [
      GetPersonRelations.REQUEST,
      GetPersonRelations.SUCCESS,
      GetPersonRelations.FAILURE
    ]
  })
  getPersonRelations: ICallAPI;

  @APICall({
    types: [
      CreatePersonRelation.REQUEST,
      CreatePersonRelation.SUCCESS,
      CreatePersonRelation.FAILURE
    ]
  })
  createPersonRelation: ICallAPI;

  @APICall({
    types: [
      DeletePersonRelation.REQUEST,
      DeletePersonRelation.SUCCESS,
      DeletePersonRelation.FAILURE
    ]
  })
  deletePersonRelation: ICallAPI;
}
