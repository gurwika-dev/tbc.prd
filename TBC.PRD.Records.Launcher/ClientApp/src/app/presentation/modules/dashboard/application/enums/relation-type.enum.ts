export enum RelationType {
  COLLEAGUE,
  ACQUAINTANCE,
  RELATIVE,
  OTHER
}
