import { IPerson } from "./get-persons.model";
import { RelationCategory } from "../enums/realation-category";
import { RelationType } from "../enums/relation-type.enum";

export interface IPersonStatistics {
  counter: number;
  type: RelationType;
  category: RelationCategory;
}

export interface IGetPersonStatisticsByIdRequest {
  id: string;
}

export interface IGetPersonStatisticsByIdResponse {
  entries: IPersonStatistics[];
}
