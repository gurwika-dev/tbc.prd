import { GenderType } from "../enums/gender-type.enum";

export class ListingQueryConfig {
  constructor(
    public page: number = 1,
    public limit: number = 10,
    public keyword: string = null,
    public isAdvancedSearch: boolean = false,
    public startBirthDate: Date = null,
    public endBirthDate: Date = null,
    public genderType: GenderType = null
  ) {}
}
