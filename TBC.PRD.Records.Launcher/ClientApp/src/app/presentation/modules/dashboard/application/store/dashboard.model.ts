import { ICity } from "../models/get-cities.model";
import { IPerson } from "../models/get-persons.model";
import { AppNormilized } from "src/app/infrastructure/store/app-normilized.model";
import { IPersonStatistics } from "../models/get-person-statistics-by-id.model";
import { IPersonRelation } from "../models/get-person-relations.model";

export interface DashboardState {
  cities: AppNormilized<ICity>;
  persons: AppNormilized<IPerson>;
  person: {
    entry: IPerson;
    statistics: IPersonStatistics[];
  };
  personRelations: AppNormilized<IPersonRelation>;
}

export const initialState: DashboardState = {
  cities: {
    entities: null,
    keys: []
  },
  persons: {
    entities: null,
    count: 0,
    keys: []
  },
  person: {
    entry: null,
    statistics: []
  },
  personRelations: {
    entities: null,
    keys: []
  }
};
