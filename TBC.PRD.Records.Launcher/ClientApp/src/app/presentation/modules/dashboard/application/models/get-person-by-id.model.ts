import { IPerson } from "./get-persons.model";

export interface IGetPersonByIdRequest {
  id: string;
}

export interface IGetPersonByIdResponse {
  entry: IPerson[];
}
