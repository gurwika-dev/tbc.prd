import { DashboardEffect } from "./dashboard.effect";
import { DashboardAction } from "./dashboard.action";

export const DASHBOARD_PROVIDERS: any[] = [DashboardAction];
export const DASHBOARD_EFFECTS: any[] = [DashboardEffect];
