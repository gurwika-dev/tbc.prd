import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { State } from "../store/app-state.model";
import { DashboardAction } from "../store/dashboard.action";
import { Store } from "@ngrx/store";

@Injectable()
export class PersonStatisticsByIdResolver implements Resolve<void> {
  constructor(
    private readonly store: Store<State>,
    protected dashboardAction: DashboardAction
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): void {
    this.store.dispatch(
      this.dashboardAction.getPersonStatisticsById.REQUEST(route.params)
    );
  }
}
