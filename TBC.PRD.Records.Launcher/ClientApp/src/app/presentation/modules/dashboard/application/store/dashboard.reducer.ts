import { AppStoreAction } from "src/app/infrastructure/store/app-store.action";
import { DashboardState, initialState } from "./dashboard.model";
import {
  GetCities,
  GetPersons,
  GetPersonById,
  GetPersonStatisticsById,
  DeletePerson,
  GetPersonRelations,
  CreatePersonRelation,
  DeletePersonRelation
} from "./dashboard.type";
import { remove, indexOf, mergeDeepRight } from "ramda";

export function dashboardReducer(
  state: DashboardState = initialState,
  action: AppStoreAction
) {
  switch (action.type) {
    case GetCities.SUCCESS:
      return mergeDeepRight(state, {
        cities: {
          entities: action.payload.entities.entries,
          keys: action.payload.result.entries
        }
      });
    case GetPersons.SUCCESS:
      return mergeDeepRight(state, {
        persons: {
          entities: action.payload.entities.entries,
          keys: action.payload.result.entries,
          count: action.payload.count
        }
      });
    case DeletePerson.SUCCESS:
      return mergeDeepRight(state, {
        persons: {
          keys: remove(
            indexOf(action.payload.id, state.persons.keys),
            1,
            state.persons.keys
          ),
          count: state.persons.count - 1
        }
      });
    case GetPersonById.SUCCESS:
      return mergeDeepRight(state, {
        person: {
          entry: action.payload.entity
        }
      });
    case GetPersonStatisticsById.SUCCESS:
      return mergeDeepRight(state, {
        person: {
          statistics: action.payload.entries
        }
      });

    case GetPersonRelations.SUCCESS:
      return mergeDeepRight(state, {
        personRelations: {
          entities: action.payload.entities.entries,
          keys: action.payload.result.entries,
          count: action.payload.count
        }
      });
    case DeletePersonRelation.SUCCESS:
      return mergeDeepRight(state, {
        personRelations: {
          keys: remove(
            indexOf(action.payload.id, state.personRelations.keys),
            1,
            state.personRelations.keys
          ),
          count: state.personRelations.count - 1
        }
      });
    default:
      return state;
  }
}
