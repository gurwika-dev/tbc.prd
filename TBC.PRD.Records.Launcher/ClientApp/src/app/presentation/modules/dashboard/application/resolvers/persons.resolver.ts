import { Injectable } from "@angular/core";
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from "@angular/router";
import { State } from "../store/app-state.model";
import { DashboardAction } from "../store/dashboard.action";
import { Store } from "@ngrx/store";
import { ListingQueryService } from "../services/listing-query.service";

@Injectable()
export class PersonsResolver implements Resolve<void> {
  constructor(
    private readonly store: Store<State>,
    private readonly dashboardAction: DashboardAction,
    private readonly listingQueryService: ListingQueryService
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): void {
    this.listingQueryService.intiQuery(route.queryParams);
    this.store.dispatch(
      this.dashboardAction.getPersons.REQUEST(
        this.listingQueryService.getQuery()
      )
    );
  }
}
