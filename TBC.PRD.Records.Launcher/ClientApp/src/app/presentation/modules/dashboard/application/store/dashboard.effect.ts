import { Injectable } from "@angular/core";
import { Store } from "@ngrx/store";
import { Effect, Actions, ofType } from "@ngrx/effects";
import { empty, of } from "rxjs";
import { mergeMap, take, map, catchError } from "rxjs/operators";
import { schema as Schema, normalize } from "normalizr";

import { DashboardAction } from "./dashboard.action";
import {
  GetCities,
  GetPersons,
  GetPersonById,
  GetPersonStatisticsById,
  CreatePerson,
  UpdatePerson,
  DeletePerson,
  GetPersonRelations,
  CreatePersonRelation,
  DeletePersonRelation
} from "./dashboard.type";
import { AppStoreAction } from "src/app/infrastructure/store/app-store.action";
import { DashboardClient } from "../clients/dashboard.client";

@Injectable()
export class DashboardEffect {
  constructor(
    private actions$: Actions,
    private action: DashboardAction,
    private client: DashboardClient
  ) {}

  @Effect()
  getCitiesRequest = this.actions$.pipe(
    ofType(GetCities.REQUEST),
    mergeMap(({ payload, onSuccess, onFailure }: AppStoreAction) =>
      this.client.getCities().pipe(
        map(res => {
          if (onSuccess) {
            onSuccess(res);
          }

          const ENTRIES_SCHEMA = [new Schema.Entity("entries", {})];
          const result = normalize(res, {
            entries: ENTRIES_SCHEMA
          });

          return this.action.getCities.SUCCESS(result);
        }),
        catchError(err => {
          if (onFailure) {
            onFailure(err);
          }
          return of(this.action.getCities.FAILURE(err));
        })
      )
    )
  );

  @Effect()
  getPersonsRequest = this.actions$.pipe(
    ofType(GetPersons.REQUEST),
    mergeMap(({ payload, onSuccess, onFailure }: AppStoreAction) =>
      this.client.getPersons(payload).pipe(
        map(res => {
          if (onSuccess) {
            onSuccess(res);
          }

          const ENTRIES_SCHEMA = [new Schema.Entity("entries", {})];
          const result = normalize(res, {
            entries: ENTRIES_SCHEMA
          });

          return this.action.getPersons.SUCCESS({ ...result, ...res });
        }),
        catchError(err => {
          if (onFailure) {
            onFailure(err);
          }
          return of(this.action.getPersons.FAILURE(err));
        })
      )
    )
  );

  @Effect()
  getPersonByIdRequest = this.actions$.pipe(
    ofType(GetPersonById.REQUEST),
    mergeMap(({ payload, onSuccess, onFailure }: AppStoreAction) =>
      this.client.getPersonById(payload).pipe(
        map(res => {
          if (onSuccess) {
            onSuccess(res);
          }

          return this.action.getPersonById.SUCCESS(res);
        }),
        catchError(err => {
          if (onFailure) {
            onFailure(err);
          }
          return of(this.action.getPersonById.FAILURE(err));
        })
      )
    )
  );

  @Effect()
  getPersonStatisticsByIdRequest = this.actions$.pipe(
    ofType(GetPersonStatisticsById.REQUEST),
    mergeMap(({ payload, onSuccess, onFailure }: AppStoreAction) =>
      this.client.getPersonStatisticsById(payload).pipe(
        map(res => {
          if (onSuccess) {
            onSuccess(res);
          }

          return this.action.getPersonStatisticsById.SUCCESS(res);
        }),
        catchError(err => {
          if (onFailure) {
            onFailure(err);
          }
          return of(this.action.getPersonStatisticsById.FAILURE(err));
        })
      )
    )
  );

  @Effect()
  getPersonRelationsRequest = this.actions$.pipe(
    ofType(GetPersonRelations.REQUEST),
    mergeMap(({ payload, onSuccess, onFailure }: AppStoreAction) =>
      this.client.getPersonRelations(payload).pipe(
        map(res => {
          if (onSuccess) {
            onSuccess(res);
          }

          const ENTRIES_SCHEMA = [new Schema.Entity("entries", {})];
          const result = normalize(res, {
            entries: ENTRIES_SCHEMA
          });

          return this.action.getPersonRelations.SUCCESS({ ...result, ...res });
        }),
        catchError(err => {
          if (onFailure) {
            onFailure(err);
          }
          return of(this.action.getPersonRelations.FAILURE(err));
        })
      )
    )
  );

  @Effect()
  createPerson = this.actions$.pipe(
    ofType(CreatePerson.REQUEST),
    mergeMap(({ payload, onSuccess, onFailure }: AppStoreAction) =>
      this.client.createPerson(payload).pipe(
        map(res => {
          if (onSuccess) {
            onSuccess(res);
          }

          return this.action.createPerson.SUCCESS(res);
        }),
        catchError(err => {
          if (onFailure) {
            onFailure(err);
          }
          return of(this.action.createPerson.FAILURE(err));
        })
      )
    )
  );

  @Effect()
  createPersonRelation = this.actions$.pipe(
    ofType(CreatePersonRelation.REQUEST),
    mergeMap(({ payload, onSuccess, onFailure }: AppStoreAction) =>
      this.client.createPersonRelation(payload).pipe(
        map(res => {
          if (onSuccess) {
            onSuccess(res);
          }

          return this.action.createPersonRelation.SUCCESS(res);
        }),
        catchError(err => {
          if (onFailure) {
            onFailure(err);
          }
          return of(this.action.createPersonRelation.FAILURE(err));
        })
      )
    )
  );

  @Effect()
  updatePerson = this.actions$.pipe(
    ofType(UpdatePerson.REQUEST),
    mergeMap(({ payload, onSuccess, onFailure }: AppStoreAction) =>
      this.client.updatePerson(payload).pipe(
        map(res => {
          if (onSuccess) {
            onSuccess(res);
          }

          return this.action.updatePerson.SUCCESS(res);
        }),
        catchError(err => {
          if (onFailure) {
            onFailure(err);
          }
          return of(this.action.updatePerson.FAILURE(err));
        })
      )
    )
  );

  @Effect()
  deletePersonRequest = this.actions$.pipe(
    ofType(DeletePerson.REQUEST),
    mergeMap(({ payload, onSuccess, onFailure }: AppStoreAction) =>
      this.client.deletePerson(payload).pipe(
        map(res => {
          if (onSuccess) {
            onSuccess(res);
          }

          return this.action.deletePerson.SUCCESS(payload);
        }),
        catchError(err => {
          if (onFailure) {
            onFailure(err);
          }
          return of(this.action.deletePerson.FAILURE(err));
        })
      )
    )
  );

  @Effect()
  deletePersonRelationsRequest = this.actions$.pipe(
    ofType(DeletePersonRelation.REQUEST),
    mergeMap(({ payload, onSuccess, onFailure }: AppStoreAction) =>
      this.client.deletePersonRelation(payload).pipe(
        map(res => {
          if (onSuccess) {
            onSuccess(res);
          }

          return this.action.deletePersonRelation.SUCCESS(payload);
        }),
        catchError(err => {
          if (onFailure) {
            onFailure(err);
          }
          return of(this.action.deletePersonRelation.FAILURE(err));
        })
      )
    )
  );
}
