import { RelationType } from "../enums/relation-type.enum";
import { RelationCategory } from "../enums/realation-category";
import { IPerson } from "./get-persons.model";

export interface IPersonRelation {
  id: string;
  personId: string;
  category: RelationCategory;
  type: RelationType;
  relatedPerson: IPerson;
}

export interface IGetPersonRelationsRequest {
  limit: number;
  page: number;
  personId: string;
}

export interface IGetPersonRelationResponse {
  entries: IPersonRelation[];
  count: number;
}
