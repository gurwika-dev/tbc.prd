import { GenderType } from "../enums/gender-type.enum";

export interface IPerson {
  id: string;
  firstName: string;
  lastName: string;
  genderType: GenderType;
  personalNumber: string;
  phoneNumber: string;
  cityId: string;
  birthDate?: Date;
  thumbnailUrl?: string;
}

export interface IGetPersonsRequest {
  limit: number;
  page: number;
  keyword?: string;
  isAdvancedSearch?: boolean;
  startBirthDate?: Date;
  endBirthDate?: Date;
  genderType?: GenderType;
}

export interface IGetPersonsResponse {
  entries: IPerson[];
  count: number;
}
