export interface ICity {
  displayName: string;
  id: string;
}

export interface IGetCitiesResponse {
  entries: ICity[];
}
