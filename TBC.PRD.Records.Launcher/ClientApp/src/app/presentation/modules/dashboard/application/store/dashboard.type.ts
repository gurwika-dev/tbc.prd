export enum GetCities {
  REQUEST = "[CITIES] Get Cities REQUEST",
  SUCCESS = "[CITIES] Get Cities SUCCESS",
  FAILURE = "[CITIES] Get Cities FAILURE"
}

export enum GetPersons {
  REQUEST = "[PERSONS] Get Persons REQUEST",
  SUCCESS = "[PERSONS] Get Persons SUCCESS",
  FAILURE = "[PERSONS] Get Persons FAILURE"
}

export enum GetPersonById {
  REQUEST = "[PERSONS] Get Person ById REQUEST",
  SUCCESS = "[PERSONS] Get Person ById SUCCESS",
  FAILURE = "[PERSONS] Get Person ById FAILURE"
}

export enum GetPersonStatisticsById {
  REQUEST = "[PERSONS] Get Person Statistics ById REQUEST",
  SUCCESS = "[PERSONS] Get Person Statistics ById SUCCESS",
  FAILURE = "[PERSONS] Get Person Statistics ById FAILURE"
}

export enum CreatePerson {
  REQUEST = "[PERSONS] Create person REQUEST",
  SUCCESS = "[PERSONS] Create person SUCCESS",
  FAILURE = "[PERSONS] Create person FAILURE"
}

export enum UpdatePerson {
  REQUEST = "[PERSONS] Update person REQUEST",
  SUCCESS = "[PERSONS] Update person SUCCESS",
  FAILURE = "[PERSONS] Update person FAILURE"
}

export enum DeletePerson {
  REQUEST = "[PERSONS] Delete person REQUEST",
  SUCCESS = "[PERSONS] Delete person SUCCESS",
  FAILURE = "[PERSONS] Delete person FAILURE"
}

export enum GetPersonRelations {
  REQUEST = "[PERSONS] Get Person Relations REQUEST",
  SUCCESS = "[PERSONS] Get Person Relations SUCCESS",
  FAILURE = "[PERSONS] Get Person Relations FAILURE"
}

export enum CreatePersonRelation {
  REQUEST = "[PERSONS] Create Person Relations REQUEST",
  SUCCESS = "[PERSONS] Create Person Relations SUCCESS",
  FAILURE = "[PERSONS] Create Person Relations FAILURE"
}

export enum DeletePersonRelation {
  REQUEST = "[PERSONS] Delete Person Relations REQUEST",
  SUCCESS = "[PERSONS] Delete Person Relations SUCCESS",
  FAILURE = "[PERSONS] Delete Person Relations FAILURE"
}
