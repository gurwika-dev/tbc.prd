import { Injectable } from "@angular/core";
import { map, join, pipe } from "ramda";
import { isNilOrEmpty } from "src/app/infrastructure/utils/is-or-not/isNilOrEmpty";
import { isNotNilOrEmpty } from "src/app/infrastructure/utils/is-or-not/isNotNilOrEmpty";
import { ListingQueryConfig } from "../models/listing-query-config.model";
import {
  tryArrayParseInt,
  tryParseInt
} from "src/app/infrastructure/utils/parser/tryArrayParseInt";

@Injectable()
export class ListingQueryService {
  public queryConfig: ListingQueryConfig;

  constructor() {}

  intiQueryConfig() {
    this.queryConfig = new ListingQueryConfig();
  }

  intiQuery(query) {
    this.intiQueryConfig();
    this.setQuery(query);
  }

  generateQueryObject(request: ListingQueryConfig): any {
    const query = {};

    Object.keys(request)
      .filter(key => isNotNilOrEmpty(request[key]) && key !== "view")
      .forEach(key => {
        query[key] = request[key];
      });

    return query;
  }

  generateQuery(request: ListingQueryConfig): string {
    let query = "";

    Object.keys(request)
      .filter(key => isNotNilOrEmpty(request[key]))
      .forEach(key => {
        if (request[key] instanceof Array) {
          query += pipe(
            map(element => {
              return key + "=" + element + "&";
            }),
            join("")
          )(request[key]);
        } else {
          query += key + "=" + request[key] + "&";
        }
      });

    return query.slice(0, -1);
  }

  setQuery(query): ListingQueryConfig {
    if (isNilOrEmpty(query)) {
      return null;
    }

    for (const key of Object.keys(query)) {
      if (this.queryConfig.hasOwnProperty(key)) {
        if (this.queryConfig[key] instanceof Array) {
          if (query[key] instanceof Array) {
            this.queryConfig[key] = tryArrayParseInt(query[key]);
          } else {
            this.queryConfig[key] = tryArrayParseInt([query[key]]);
          }
        } else {
          this.queryConfig[key] = tryParseInt(query[key]);
        }
      }
    }

    return this.queryConfig;
  }

  getQuery(): ListingQueryConfig {
    return this.queryConfig;
  }

  getPage(): number {
    return this.queryConfig.page;
  }

  getFiled(filedName: string) {
    return this.queryConfig[filedName];
  }
}
