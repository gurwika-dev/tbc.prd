import { AppState } from "src/app/infrastructure/store/app-state.model";
import { DashboardState } from "./dashboard.model";

export const FEATURE_NAME = "dashboard";

export interface State extends AppState {
  dashboard: DashboardState;
}
