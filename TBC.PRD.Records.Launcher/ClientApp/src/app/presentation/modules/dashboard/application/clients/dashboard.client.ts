import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { IGetCitiesResponse } from "../models/get-cities.model";
import { Injectable } from "@angular/core";
import {
  IGetPersonsResponse,
  IGetPersonsRequest,
  IPerson
} from "../models/get-persons.model";
import {
  IGetPersonByIdRequest,
  IGetPersonByIdResponse
} from "../models/get-person-by-id.model";
import {
  IGetPersonStatisticsByIdResponse,
  IGetPersonStatisticsByIdRequest
} from "../models/get-person-statistics-by-id.model";
import { isNotNilOrEmpty } from "src/app/infrastructure/utils/is-or-not/isNotNilOrEmpty";
import {
  IPersonRelation,
  IGetPersonRelationResponse,
  IGetPersonRelationsRequest
} from "../models/get-person-relations.model";

@Injectable()
export class DashboardClient {
  constructor(private readonly httpClient: HttpClient) {}

  getCities(): Observable<IGetCitiesResponse> {
    return this.httpClient.get<IGetCitiesResponse>(`/cities`);
  }

  getPersons(request: IGetPersonsRequest): Observable<IGetPersonsResponse> {
    // Initialize Params Object
    let params = new HttpParams();

    // Begin assigning parameters
    params = params.append("limit", `${request.limit}`);
    params = params.append("page", `${request.page}`);

    isNotNilOrEmpty(request.keyword) &&
      (params = params.append("keyword", request.keyword));
    isNotNilOrEmpty(request.isAdvancedSearch) &&
      (params = params.append(
        "isAdvancedSearch",
        `${request.isAdvancedSearch}`
      ));
    isNotNilOrEmpty(request.startBirthDate) &&
      (params = params.append("startBirthDate", `${request.startBirthDate}`));
    isNotNilOrEmpty(request.endBirthDate) &&
      (params = params.append("endBirthDate", `${request.endBirthDate}`));
    isNotNilOrEmpty(request.genderType) &&
      (params = params.append("genderType", `${request.genderType}`));

    return this.httpClient.get<IGetPersonsResponse>(`/persons`, {
      params
    });
  }

  getPersonById(
    request: IGetPersonByIdRequest
  ): Observable<IGetPersonByIdResponse> {
    return this.httpClient.get<IGetPersonByIdResponse>(
      `/persons/${request.id}`
    );
  }

  getPersonStatisticsById(
    request: IGetPersonStatisticsByIdRequest
  ): Observable<IGetPersonStatisticsByIdResponse> {
    return this.httpClient.get<IGetPersonStatisticsByIdResponse>(
      `/persons/${request.id}/statistics`
    );
  }

  getPersonRelations(
    request: IGetPersonRelationsRequest
  ): Observable<IGetPersonRelationResponse> {
    // Initialize Params Object
    let params = new HttpParams();

    // Begin assigning parameters
    params = params.append("limit", `${request.limit}`);
    params = params.append("page", `${request.page}`);

    return this.httpClient.get<IGetPersonRelationResponse>(
      `/persons/${request.personId}/relations`,
      {
        params
      }
    );
  }

  createPerson(request: IPerson): Observable<void> {
    return this.httpClient.post<void>(`/persons`, request);
  }

  createPersonRelation(request: IPersonRelation): Observable<void> {
    return this.httpClient.post<void>(
      `/persons/${request.personId}/relations`,
      request
    );
  }

  updatePerson(request: IPerson): Observable<void> {
    return this.httpClient.put<void>(`/persons/${request.id}`, request);
  }

  uploadPhoto(file: File, personsId: string, fileId: string): Observable<void> {
    const form: FormData = new FormData();
    form.append("photo", file);
    form.append("fileId", fileId);

    return this.httpClient.post<void>(`/persons/${personsId}/thumbnail`, form);
  }

  deletePerson(request: IPerson): Observable<void> {
    return this.httpClient.delete<void>(`/persons/${request.id}`);
  }

  deletePersonRelation(request: IPersonRelation): Observable<void> {
    return this.httpClient.delete<void>(
      `/persons/${request.personId}/relations/${request.id}`
    );
  }
}
