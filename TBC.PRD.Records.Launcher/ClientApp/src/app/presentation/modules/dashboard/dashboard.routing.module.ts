import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { DashboardComponent } from "./pages/Dashboard/Dashboard.component";
import { ListingComponent } from "./pages/Listing/Listing.component";
import { CitiesResolver } from "./application/resolvers/cities.resolver";
import { PersonsResolver } from "./application/resolvers/persons.resolver";
import { ViewComponent } from "./pages/View/View.component";
import { PersonByIdResolver } from "./application/resolvers/person-by-id.resolver";
import { PersonStatisticsByIdResolver } from "./application/resolvers/person-statistics-by-id.reslover";
import { UpsertComponent } from "./pages/Upsert/Upsert.component";
import { PersonRelationsResolver } from "./application/resolvers/person-relations.resolver";

const routes: Routes = [
  {
    path: "",
    component: DashboardComponent,
    resolve: { cities: CitiesResolver },
    children: [
      {
        path: "",
        component: ListingComponent,
        resolve: { persons: PersonsResolver }
      },
      {
        pathMatch: "full",
        path: "persons",
        component: UpsertComponent,
        resolve: {}
      },
      {
        pathMatch: "full",
        path: "persons/:id",
        component: UpsertComponent,
        resolve: {
          person: PersonByIdResolver,
          personRelations: PersonRelationsResolver
        }
      },
      {
        pathMatch: "full",
        path: "persons/:id/view",
        component: ViewComponent,
        resolve: {
          person: PersonByIdResolver,
          personRelations: PersonRelationsResolver,
          statistics: PersonStatisticsByIdResolver
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashbaordRoutingModule {}
