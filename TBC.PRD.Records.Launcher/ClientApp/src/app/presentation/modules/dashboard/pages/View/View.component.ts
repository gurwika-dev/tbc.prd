import { Component } from "@angular/core";
import {
  IUnsubscribeOnDestroy,
  UnsubscribeOnDestroy
} from "src/app/infrastructure/decorators/UnsubscribeOnDestroy";
import { Subject } from "rxjs";
import { State } from "../../application/store/app-state.model";
import { Store, select } from "@ngrx/store";
import { takeUntil, filter } from "rxjs/operators";
import { isNotNilOrEmpty } from "src/app/infrastructure/utils/is-or-not/isNotNilOrEmpty";
import { IPerson } from "../../application/models/get-persons.model";
import { IPersonStatistics } from "../../application/models/get-person-statistics-by-id.model";
import { ISelectOption } from "src/app/presentation/componenets/select/select-option.model";
import { generateOptions } from "src/app/infrastructure/utils/generate/generateOptions";
import { GenderType } from "../../application/enums/gender-type.enum";
import { AppNormilized } from "src/app/infrastructure/store/app-normilized.model";
import { ICity } from "../../application/models/get-cities.model";
import { FormControl, FormGroup, FormBuilder } from "@angular/forms";
import { environment } from "src/environments/environment";
import { IPersonRelation } from "../../application/models/get-person-relations.model";
import { PaginationService } from "src/app/application/services/pagination.service";
import { DashboardAction } from "../../application/store/dashboard.action";
import { IPagination } from "src/app/presentation/componenets/pagination/pagination.model";

@UnsubscribeOnDestroy()
@Component({
  selector: "app-view",
  templateUrl: "./View.component.html",
  styleUrls: ["./View.component.scss"]
})
export class ViewComponent implements IUnsubscribeOnDestroy {
  public destroy$: Subject<boolean>;

  public person: IPerson;
  public statistics: IPersonStatistics[];
  public cities: AppNormilized<ICity>;
  public personRelations: AppNormilized<IPersonRelation>;
  public listPagination: Array<IPagination>;

  public relationsPage = 1;
  public relationsLimit = 10;

  get baseUrl() {
    return environment.cdnBaseUrl;
  }

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly store: Store<State>,
    private readonly paginationService: PaginationService,
    private readonly dashboardAction: DashboardAction
  ) {}

  ngOnInit() {
    this.subscribePerson();
    this.subscribeCities();
    this.subscribePersonStatistics();
    this.subscribePersonRelations();
  }

  ngOnDestroy() {}

  subscribePerson() {
    this.store
      .pipe(
        select(store => store.dashboard.person.entry),
        takeUntil(this.destroy$),
        filter(isNotNilOrEmpty)
      )
      .subscribe(state => {
        this.person = state;
      });
  }

  subscribePersonStatistics() {
    this.store
      .pipe(
        select(store => store.dashboard.person.statistics),
        takeUntil(this.destroy$),
        filter(isNotNilOrEmpty)
      )
      .subscribe(state => {
        this.statistics = state;
      });
  }

  subscribeCities() {
    this.store
      .pipe(
        select(store => store.dashboard.cities),
        takeUntil(this.destroy$),
        filter(isNotNilOrEmpty)
      )
      .subscribe(state => {
        this.cities = state;
      });
  }

  subscribePersonRelations() {
    this.store
      .pipe(
        select(store => store.dashboard.personRelations),
        takeUntil(this.destroy$),
        filter(isNotNilOrEmpty)
      )
      .subscribe(state => {
        this.personRelations = state;
        this.handlePaginationChange();
      });
  }

  handlePaginationChange() {
    this.listPagination = this.paginationService.getPagination(
      this.relationsPage,
      this.personRelations.count,
      this.relationsLimit
    );
  }

  handlePageChange(page) {
    this.relationsPage = page;
    this.store.dispatch(
      this.dashboardAction.getPersonRelations.REQUEST({
        limit: this.relationsLimit,
        page: this.relationsPage,
        personId: this.person.id
      })
    );
  }
}
