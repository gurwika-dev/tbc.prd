import { Component, ElementRef, ViewChild } from "@angular/core";
import {
  IUnsubscribeOnDestroy,
  UnsubscribeOnDestroy
} from "src/app/infrastructure/decorators/UnsubscribeOnDestroy";
import { Subject } from "rxjs";
import { State } from "../../application/store/app-state.model";
import { Store, select } from "@ngrx/store";
import { takeUntil, filter, finalize } from "rxjs/operators";
import { isNotNilOrEmpty } from "src/app/infrastructure/utils/is-or-not/isNotNilOrEmpty";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import { generateOptions } from "src/app/infrastructure/utils/generate/generateOptions";
import { GenderType } from "../../application/enums/gender-type.enum";
import { RelationType } from "../../application/enums/relation-type.enum";

import { PhoneNumberType } from "../../application/enums/phone-number-type.enum";
import { ISelectOption } from "src/app/presentation/componenets/select/select-option.model";
import { DashboardAction } from "../../application/store/dashboard.action";

import * as uuid from "uuid";
import { Patterns } from "src/app/infrastructure/patters/patterns";
import { environment } from "src/environments/environment";
import { DashboardClient } from "../../application/clients/dashboard.client";
import { ListingMode } from "../../application/enums/listing-mode";
import { IPersonRelation } from "../../application/models/get-person-relations.model";
import { AppNormilized } from "src/app/infrastructure/store/app-normilized.model";
import { scollUp } from "src/app/infrastructure/utils/scrollUp";
import { DeleteDialogComponent } from "../../dialogs/delete/delete-dialog.component";
import { MatDialog } from "@angular/material";
import { propertyNotEqualRequired } from "src/app/infrastructure/validators/propertyNotEqualRequired";
import { ErrorDialogComponent } from "../../dialogs/error/error-dialog.component";
import { IPagination } from "src/app/presentation/componenets/pagination/pagination.model";
import { PaginationService } from "src/app/application/services/pagination.service";

@UnsubscribeOnDestroy()
@Component({
  selector: "app-upsert",
  templateUrl: "./Upsert.component.html",
  styleUrls: ["./Upsert.component.scss"]
})
export class UpsertComponent implements IUnsubscribeOnDestroy {
  @ViewChild("personRelationsRef", { static: true })
  personRelationsRef: ElementRef;

  public destroy$: Subject<boolean>;

  public personRelations: AppNormilized<IPersonRelation>;

  public genderTypeOptions: Array<ISelectOption> = [];
  public relationTypeOptions: Array<ISelectOption> = [];
  public phoneNumberTypeOptions: Array<ISelectOption> = [];
  public citiesOptions: Array<ISelectOption> = [];
  public listPagination: Array<IPagination>;

  public formGroup: FormGroup;
  public relationsFormGroup: FormGroup;

  public submitted = false;
  public submittedRelations = false;
  public showLoader = false;
  public showRelationsLoader = false;

  public relationsPage = 1;
  public relationsLimit = 10;
  public isPhotoLoading = false;
  public mode = ListingMode.ADD;

  get startDate() {
    const date = new Date();
    date.setDate(date.getDate() - 365 * 18);
    return date;
  }

  get baseUrl() {
    return environment.cdnBaseUrl;
  }

  constructor(
    private readonly store: Store<State>,
    private readonly formBuilder: FormBuilder,
    private readonly dashboardAction: DashboardAction,
    private readonly dashboardClient: DashboardClient,
    private readonly paginationService: PaginationService,
    private readonly dialog: MatDialog
  ) {}

  ngOnInit() {
    this.buildData();
    this.buildForm();
    this.buildRelationsForm();
    this.subscribeCities();
    this.subscribePerson();
    this.subscribePersonRelations();
  }

  ngOnDestroy() {}

  buildData() {
    this.genderTypeOptions = generateOptions({ GenderType });
    this.phoneNumberTypeOptions = generateOptions({ PhoneNumberType });
    this.relationTypeOptions = generateOptions({ RelationType });
  }

  buildForm() {
    const personId = uuid.v4();
    this.formGroup = this.formBuilder.group({
      id: new FormControl(personId, [Validators.required]),
      firstName: new FormControl(null, [
        Validators.required,
        Validators.pattern(Patterns.latinOrGeorgian)
      ]),
      lastName: new FormControl(null, [
        Validators.required,
        Validators.pattern(Patterns.latinOrGeorgian)
      ]),
      genderType: new FormControl(null, [Validators.required]),
      personalNumber: new FormControl(null, [
        Validators.required,
        Validators.pattern(Patterns.personalNumber)
      ]),

      phoneNumber: new FormGroup({
        type: new FormControl(null, [Validators.required]),
        number: new FormControl(null, [
          Validators.required,
          Validators.pattern(Patterns.mobile),
          Validators.minLength(environment.validator.minPhoneNumber),
          Validators.maxLength(environment.validator.maxPhoneNumber)
        ])
      }),

      cityId: new FormControl(null, [Validators.required]),
      thumbnailUrl: new FormControl(null, [Validators.required]),
      birthDate: new FormControl(null, [Validators.required])
    });
  }

  buildRelationsForm() {
    this.relationsFormGroup = this.formBuilder.group({
      relatedPersonId: new FormControl(null, [
        Validators.required,
        propertyNotEqualRequired(
          () => this.relationsFormGroup,
          this.formGroup.value.id
        )
      ]),
      type: new FormControl(null, [Validators.required])
    });
  }

  subscribeCities() {
    this.store
      .pipe(
        select(store => store.dashboard.cities),
        takeUntil(this.destroy$),
        filter(isNotNilOrEmpty)
      )
      .subscribe(state => {
        this.citiesOptions = state.keys.map(element => ({
          text: state.entities[element].displayName,
          value: element
        }));
      });
  }

  subscribePerson() {
    this.store
      .pipe(
        select(store => store.dashboard.person.entry),
        takeUntil(this.destroy$),
        filter(isNotNilOrEmpty)
      )
      .subscribe(state => {
        this.mode = ListingMode.EDIT;
        this.formGroup.setValue({
          id: state.id,
          firstName: state.firstName,
          lastName: state.lastName,
          genderType: state.genderType,
          personalNumber: state.personalNumber,
          birthDate: state.birthDate,
          thumbnailUrl: state.thumbnailUrl,
          cityId: state.cityId,
          phoneNumber: {
            type: state.phoneNumber.split("-")[0],
            number: state.phoneNumber.split("-")[1]
          }
        });

        this.relationsFormGroup
          .get("relatedPersonId")
          .setValidators(
            propertyNotEqualRequired(
              () => this.relationsFormGroup,
              this.formGroup.value.id
            )
          );
      });
  }

  subscribePersonRelations() {
    this.store
      .pipe(
        select(store => store.dashboard.personRelations),
        takeUntil(this.destroy$),
        filter(isNotNilOrEmpty)
      )
      .subscribe(state => {
        this.personRelations = state;
        this.handlePaginationChange();
      });
  }

  openDelete(personRelation: IPersonRelation): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: "450px",
      data: {}
    });

    dialogRef.afterClosed().subscribe(handleDelete => {
      if (handleDelete) {
        this.store.dispatch(
          this.dashboardAction.deletePersonRelation.REQUEST(
            personRelation,
            () => {},
            exeption => {
              this.dialog.open(ErrorDialogComponent, {
                width: "450px",
                data: exeption.data
              });
            }
          )
        );
      }
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      this.showLoader = true;

      const $action =
        this.mode == ListingMode.EDIT
          ? this.dashboardAction.updatePerson
          : this.dashboardAction.createPerson;

      this.store.dispatch(
        $action.REQUEST(
          {
            ...this.formGroup.value,
            ...{
              phoneNumber:
                this.formGroup.value.phoneNumber.type +
                "-" +
                this.formGroup.value.phoneNumber.number
            }
          },
          () => {
            this.showLoader = false;
            this.mode = ListingMode.EDIT;

            scollUp(
              this.personRelationsRef.nativeElement,
              window,
              false,
              500,
              "linear",
              () => {}
            );
          },
          exeption => {
            this.showLoader = false;
            this.dialog.open(ErrorDialogComponent, {
              width: "450px",
              data: exeption.data
            });
          }
        )
      );
    }
  }

  onSubmitRelations() {
    this.submittedRelations = true;
    if (this.relationsFormGroup.valid) {
      this.showRelationsLoader = true;

      this.store.dispatch(
        this.dashboardAction.createPersonRelation.REQUEST(
          {
            ...this.relationsFormGroup.value,
            personId: this.formGroup.value.id
          },
          () => {
            this.handlePageChange(1);
            this.submittedRelations = false;
            this.showRelationsLoader = false;
            this.relationsFormGroup.reset();
          },
          exeption => {
            this.showRelationsLoader = false;
            this.dialog.open(ErrorDialogComponent, {
              width: "450px",
              data: exeption.data
            });
          }
        )
      );
    }
  }

  handlePaginationChange() {
    this.listPagination = this.paginationService.getPagination(
      this.relationsPage,
      this.personRelations.count,
      this.relationsLimit
    );
  }

  handlePageChange(page) {
    this.relationsPage = page;
    this.store.dispatch(
      this.dashboardAction.getPersonRelations.REQUEST({
        limit: this.relationsLimit,
        page: this.relationsPage,
        personId: this.formGroup.value.id
      })
    );
  }

  uploadPhoto($event) {
    this.isPhotoLoading = true;
    const fileId = uuid.v4();

    this.dashboardClient
      .uploadPhoto($event, this.formGroup.value.id, fileId)
      .pipe(
        takeUntil(this.destroy$),
        finalize(() => {
          this.isPhotoLoading = false;
        })
      )
      .subscribe(() => {
        this.formGroup
          .get("thumbnailUrl")
          .setValue(fileId + "." + $event.name.split(".").pop());
      });
  }
}
