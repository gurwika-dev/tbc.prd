import { Component } from "@angular/core";
import {
  IUnsubscribeOnDestroy,
  UnsubscribeOnDestroy
} from "src/app/infrastructure/decorators/UnsubscribeOnDestroy";
import { Subject } from "rxjs";
import { State } from "../../application/store/app-state.model";
import { Store, select } from "@ngrx/store";
import { takeUntil, filter } from "rxjs/operators";
import { isNotNilOrEmpty } from "src/app/infrastructure/utils/is-or-not/isNotNilOrEmpty";
import { AppNormilized } from "src/app/infrastructure/store/app-normilized.model";
import { IPerson } from "../../application/models/get-persons.model";
import { ICity } from "../../application/models/get-cities.model";
import { FormBuilder, FormGroup, FormControl } from "@angular/forms";
import { ISelectOption } from "src/app/presentation/componenets/select/select-option.model";
import { generateOptions } from "src/app/infrastructure/utils/generate/generateOptions";
import { GenderType } from "../../application/enums/gender-type.enum";
import { PaginationService } from "src/app/application/services/pagination.service";
import { IPagination } from "src/app/presentation/componenets/pagination/pagination.model";
import { DashboardAction } from "../../application/store/dashboard.action";
import { ListingQueryService } from "../../application/services/listing-query.service";
import { Router } from "@angular/router";
import { MatDialog } from "@angular/material";
import { DeleteDialogComponent } from "../../dialogs/delete/delete-dialog.component";
import { ErrorDialogComponent } from "../../dialogs/error/error-dialog.component";

@UnsubscribeOnDestroy()
@Component({
  selector: "app-listing",
  templateUrl: "./listing.component.html",
  styleUrls: ["./listing.component.scss"]
})
export class ListingComponent implements IUnsubscribeOnDestroy {
  public destroy$: Subject<boolean>;

  public formGroup: FormGroup;
  public persons: AppNormilized<IPerson>;

  public genderTypeOptions: Array<ISelectOption> = [];
  public listPagination: Array<IPagination>;

  constructor(
    private readonly router: Router,
    private readonly paginationService: PaginationService,
    private readonly formBuilder: FormBuilder,
    private readonly store: Store<State>,
    private readonly dashboardAction: DashboardAction,
    private readonly listingQueryService: ListingQueryService,
    private readonly dialog: MatDialog
  ) {}

  ngOnInit() {
    this.buildData();
    this.buildForm();
    this.subscribePersons();
    this.subscribeForm();
  }

  ngOnDestroy() {}

  buildForm() {
    this.formGroup = this.formBuilder.group({
      page: [this.listingQueryService.getPage(), []],
      limit: [this.listingQueryService.getFiled("limit"), []],
      keyword: [this.listingQueryService.getFiled("keyword"), []],
      isAdvancedSearch: [
        this.listingQueryService.getFiled("isAdvancedSearch"),
        []
      ],
      genderType: new FormControl(
        this.listingQueryService.getFiled("genderType"),
        []
      )
    });
  }

  buildData() {
    this.genderTypeOptions = generateOptions({ GenderType });
  }

  subscribeForm() {
    this.formGroup.controls["isAdvancedSearch"].valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        if (value) {
          this.formGroup.get("genderType").enable({ emitEvent: false });
        } else {
          this.formGroup.get("genderType").reset({ emitEvent: false });
          this.formGroup.get("genderType").disable({ emitEvent: false });
        }
      });

    this.formGroup.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe(value => {
        this.onSubmit();
      });
  }

  subscribePersons() {
    this.store
      .pipe(
        select(store => store.dashboard.persons),
        takeUntil(this.destroy$),
        filter(isNotNilOrEmpty)
      )
      .subscribe(state => {
        this.persons = state;
        this.handlePaginationChange();
      });
  }

  onSubmit() {
    this.store.dispatch(
      this.dashboardAction.getPersons.REQUEST(this.formGroup.value)
    );
    this.updateRouteParams();
  }

  openDelete(person: IPerson): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent, {
      width: "450px",
      data: {}
    });

    dialogRef.afterClosed().subscribe(handleDelete => {
      if (handleDelete) {
        this.store.dispatch(
          this.dashboardAction.deletePerson.REQUEST(
            person,
            () => {},
            exeption => {
              this.dialog.open(ErrorDialogComponent, {
                width: "450px",
                data: exeption.data
              });
            }
          )
        );
      }
    });
  }

  handlePaginationChange() {
    this.listPagination = this.paginationService.getPagination(
      this.formGroup.value.page,
      this.persons.count,
      this.formGroup.value.limit
    );
  }

  handlePageChange(page) {
    this.formGroup.get("page").setValue(page);
  }

  updateRouteParams() {
    this.router.navigate(["/"], {
      queryParams: this.listingQueryService.generateQueryObject(
        this.formGroup.value
      )
    });
  }
}
