import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { DashbaordRoutingModule } from "./dashboard.routing.module";
import { LocalizationForChildModule } from "../../../infrastructure/localization/localization.module";

import { DashboardComponent } from "./pages/Dashboard/Dashboard.component";
import { ListingComponent } from "./pages/Listing/Listing.component";
import { NavigationModule } from "../../containers/navigation/navigation.module";
import { StoreModule } from "@ngrx/store";
import { EffectsModule } from "@ngrx/effects";
import { dashboardReducer } from "./application/store/dashboard.reducer";
import { FEATURE_NAME } from "./application/store/app-state.model";
import { DASHBOARD_EFFECTS, DASHBOARD_PROVIDERS } from "./application/store";
import { DashboardClient } from "./application/clients/dashboard.client";
import { CitiesResolver } from "./application/resolvers/cities.resolver";
import { PersonsResolver } from "./application/resolvers/persons.resolver";
import { DataTableModule } from "../../containers/data-table/data-table.module";
import { BoxModule } from "../../containers/box/box.module";
import { ButtonModule } from "../../componenets/button/button.module";
import { ViewComponent } from "./pages/View/View.component";
import { PersonByIdResolver } from "./application/resolvers/person-by-id.resolver";
import { PersonStatisticsByIdResolver } from "./application/resolvers/person-statistics-by-id.reslover";
import { UpsertComponent } from "./pages/Upsert/Upsert.component";
import { HeaderModule } from "../../containers/header/header.module";
import { TextInputModule } from "../../componenets/text-input/text-input.module";
import { CheckBoxModule } from "../../componenets/checkbox/checkbox.module";
import { SelectModule } from "../../componenets/select/select.module";
import { PaginationModule } from "../../componenets/pagination/pagination.module";

import {
  MatDatepickerModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatInputModule,
  MatDialogModule
} from "@angular/material";
import { FieldErrorModule } from "../../componenets/field-error/field-error.module";
import { DirectivesModule } from "src/app/infrastructure/directives";
import { ListingQueryService } from "./application/services/listing-query.service";
import { DeleteDialogComponent } from "./dialogs/delete/delete-dialog.component";
import { PersonRelationsResolver } from "./application/resolvers/person-relations.resolver";
import { ErrorDialogComponent } from "./dialogs/error/error-dialog.component";

const SDK_PROVIDERS: Array<any> = [DASHBOARD_PROVIDERS];
const SDK_EFFECTS: any[] = [...DASHBOARD_EFFECTS];

const MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  BoxModule,
  ButtonModule,
  PaginationModule,
  SelectModule,
  CheckBoxModule,
  TextInputModule,
  HeaderModule,
  DataTableModule,
  NavigationModule,
  DashbaordRoutingModule,
  LocalizationForChildModule,
  FieldErrorModule,
  DirectivesModule,

  MatDialogModule,
  MatDatepickerModule,
  MatFormFieldModule,
  MatNativeDateModule,
  MatInputModule,

  // Store
  StoreModule.forFeature(FEATURE_NAME, dashboardReducer),
  EffectsModule.forFeature(SDK_EFFECTS)
];

const DECLARATIONS = [
  ListingComponent,
  DashboardComponent,
  ViewComponent,
  UpsertComponent,
  DeleteDialogComponent,
  ErrorDialogComponent
];

const PROVIDERS = [
  SDK_PROVIDERS,
  DashboardClient,
  CitiesResolver,
  PersonsResolver,
  PersonByIdResolver,
  PersonStatisticsByIdResolver,
  PersonRelationsResolver,
  ListingQueryService
];

const ENTRY_COMPONENETS = [DeleteDialogComponent, ErrorDialogComponent];

@NgModule({
  declarations: DECLARATIONS,
  imports: MODULES,
  providers: PROVIDERS,
  entryComponents: ENTRY_COMPONENETS,
  exports: []
})
export class DashboardModule {}
