import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { IdentityClient } from "./application/clients/identity.client";
import { SignInComponent } from "./pages/sign-in/sign-in.component";
import { SignOutComponent } from "./pages/sing-out/sign-out.component";

const routes: Routes = [
  {
    path: "",
    component: SignInComponent,
    pathMatch: "full"
  },
  {
    path: "sign-out",
    component: SignOutComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [IdentityClient]
})
export class IdentityRoutingModule {}
