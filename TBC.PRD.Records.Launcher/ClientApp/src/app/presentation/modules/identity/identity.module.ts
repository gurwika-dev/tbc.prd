import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { IdentityRoutingModule } from "./identity.routing.module";
import { LocalizationForChildModule } from "../../../infrastructure/localization/localization.module";
import { SignInComponent } from "./pages/sign-in/sign-in.component";
import { TextInputModule } from "../../componenets/text-input/text-input.module";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { ButtonModule } from "../../componenets/button/button.module";
import { FieldErrorModule } from "../../componenets/field-error/field-error.module";
import { SignOutComponent } from "./pages/sing-out/sign-out.component";

const MODULES = [
  FormsModule,
  ReactiveFormsModule,
  CommonModule,
  IdentityRoutingModule,
  LocalizationForChildModule,
  TextInputModule,
  FieldErrorModule,
  ButtonModule,
  CommonModule,
  RouterModule
];

const DECLARATIONS = [SignInComponent, SignOutComponent];

const PROVIDERS = [];

@NgModule({
  declarations: DECLARATIONS,
  imports: MODULES,
  providers: PROVIDERS,
  exports: []
})
export class IdentityModule {}
