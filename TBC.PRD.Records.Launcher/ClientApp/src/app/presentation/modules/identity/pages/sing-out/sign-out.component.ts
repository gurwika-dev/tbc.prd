import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "src/app/application/services/auth.service";

@Component({
  selector: "app-sign-out",
  templateUrl: "./sign-out.component.html",
  styleUrls: ["./sign-out.component.scss"]
})
export class SignOutComponent {
  constructor(
    private readonly authService: AuthService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.authService.signOut().subscribe(() => {
      this.router.navigate(["/identity"]);
    });
  }
}
