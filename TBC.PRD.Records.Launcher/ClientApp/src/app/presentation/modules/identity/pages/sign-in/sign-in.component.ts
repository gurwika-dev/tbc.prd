import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { IdentityClient } from "../../application/clients/identity.client";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: "app-sign-in",
  templateUrl: "./sign-in.component.html",
  styleUrls: ["./sign-in.component.scss"]
})
export class SignInComponent {
  public formGroup: FormGroup;
  public submitted = false;
  public showLoader = false;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly identityClient: IdentityClient,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.formGroup = this.formBuilder.group({
      username: ["", [Validators.required]],
      password: ["", Validators.required]
    });
  }

  onSubmit() {
    this.submitted = true;
    if (this.formGroup.valid) {
      this.showLoader = true;
      this.identityClient.signIn(this.formGroup.value).subscribe(() => {
        this.router.navigate(["/"]);
      });
    }
  }
}
