export interface ISignInRequest {
  username: string;
  password: string;
}

export interface ISignInResponse {
  access_token: string;
  expires_in: number;
  token_type: string;
  scope: string;
}
