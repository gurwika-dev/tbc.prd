import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { ISignInRequest, ISignInResponse } from "../models/sign-in.model";
import { tap } from "rxjs/operators";
import { AuthService } from "src/app/application/services/auth.service";

@Injectable()
export class IdentityClient {
  get apiUrl() {
    return `${environment.identity.apiurl}/connect/token`;
  }

  get authToken() {
    return btoa(
      `${environment.identity.clientId}:${environment.identity.clientSercret}`
    );
  }

  get headers() {
    return {
      Authorization: `Basic ${this.authToken}`,
      "Content-Type": "application/x-www-form-urlencoded"
    };
  }

  constructor(
    private readonly httpClient: HttpClient,
    private readonly authService: AuthService
  ) {}

  signIn(request: ISignInRequest) {
    const body = new HttpParams()
      .set("username", request.username)
      .set("password", request.password)
      .set("grant_type", "password")
      .set("scope", "TBC.PRD.Identity.LauncherAPI TBC.PRD.Records.LauncherAPI");

    return this.httpClient
      .post<ISignInResponse>(this.apiUrl, body.toString(), {
        headers: this.headers
      })
      .pipe(
        tap(res => {
          this.authService.setState(res.access_token);
        })
      );
  }
}
