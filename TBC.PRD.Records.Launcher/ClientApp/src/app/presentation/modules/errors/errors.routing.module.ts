import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { Error404Component } from "./pages/404/error-404.component";

const routes: Routes = [
  {
    path: "",
    component: Error404Component,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ErrorsRoutingModule {}
