import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Error404Component } from "./pages/404/error-404.component";
import { ErrorsRoutingModule } from "./errors.routing.module";
import { LocalizationForChildModule } from "../../../infrastructure/localization/localization.module";

const MODULES = [CommonModule, ErrorsRoutingModule, LocalizationForChildModule];

const DECLARATIONS = [Error404Component];

const PROVIDERS = [];

@NgModule({
  declarations: DECLARATIONS,
  imports: MODULES,
  providers: PROVIDERS,
  exports: [Error404Component]
})
export class ErrorsModule {}
