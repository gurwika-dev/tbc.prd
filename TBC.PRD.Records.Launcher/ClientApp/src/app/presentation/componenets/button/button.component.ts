import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-button",
  templateUrl: "./button.component.html",
  styleUrls: ["./button.component.scss"]
})
export class ButtonComponent {
  @Input()
  text = "";

  @Input()
  type = "black";

  @Input()
  isLoading = false;

  @Input()
  disable = false;

  @Output()
  press = new EventEmitter();

  @Input()
  showLinkIcon = false;

  get className() {
    return `button-input button-input--${this.type} ${
      this.disable ? "button-input--disabled" : ""
    }`;
  }

  onClick() {
    this.press.emit();
  }

  constructor() {}
}
