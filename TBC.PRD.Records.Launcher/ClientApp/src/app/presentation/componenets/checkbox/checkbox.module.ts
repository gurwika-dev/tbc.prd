import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { LocalizationForChildModule } from "src/app/infrastructure/localization/localization.module";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { DirectivesModule } from "src/app/infrastructure/directives";
import { CheckboxComponent } from "./checkbox.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DirectivesModule,
    LocalizationForChildModule,
    ReactiveFormsModule
  ],
  declarations: [CheckboxComponent],
  exports: [CheckboxComponent]
})
export class CheckBoxModule {}
