import { Component, forwardRef, Input, Output, EventEmitter } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'app-checkbox',
  templateUrl: './checkbox.component.html',
  styleUrls: ['./checkbox.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CheckboxComponent),
      multi: true,
    },
  ],
})
export class CheckboxComponent implements ControlValueAccessor {
  @Input()
  label?: string = null;

  @Input()
  name?: string;

  @Input()
  disabled?: boolean;

  @Output()
  uncheck = new EventEmitter();

  @Output()
  prevent = new EventEmitter();

  value = false;

  onChange: (_: any) => void = () => {};
  onTouched: () => void = () => {};

  constructor() {}

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  updateChanges() {
    this.onChange(this.value);
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  toggleCheck() {
    if (!this.disabled) {
      this.value = !this.value;

      if (!this.value) {
        this.uncheck.emit();
      }

      this.updateChanges();
    }
  }
}
