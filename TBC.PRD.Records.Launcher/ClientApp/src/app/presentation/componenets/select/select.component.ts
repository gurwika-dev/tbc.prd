import {
  Component,
  Input,
  forwardRef,
  ViewChild,
  ElementRef,
  OnChanges,
  AfterViewInit,
  Injector,
  SimpleChanges
} from "@angular/core";
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  FormControl,
  NgControl
} from "@angular/forms";
import { pipe, equals, path, not } from "ramda";
import { TranslateService } from "@ngx-translate/core";
import { isNotNilOrEmpty } from "src/app/infrastructure/utils/is-or-not/isNotNilOrEmpty";
import { ISelectOption } from "./select-option.model";

@Component({
  selector: "app-select",
  templateUrl: "./select.component.html",
  styleUrls: ["./select.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SelectComponent),
      multi: true
    }
  ]
})
export class SelectComponent
  implements ControlValueAccessor, AfterViewInit, OnChanges {
  @ViewChild("positionDiv", { static: false }) positionDiv: ElementRef;

  @Input()
  _value: number | string;
  _options: Array<ISelectOption> = [];
  visibleOptions: Array<ISelectOption> = [];

  set disabled(value: boolean) {
    this._disabled = value;
    this.searchValue = "";
  }

  get disabled() {
    return this._disabled || this.options.length === 0;
  }
  @Input()
  _disabled: boolean;

  @Input()
  selected?: ISelectOption = null;

  @Input()
  placeholder = "";

  @Input()
  label?: string;

  @Input()
  prefix?: string;

  @Input()
  invalid?: boolean;

  @Input()
  searchable?: boolean;

  @Input()
  showErrors = false;

  @Input()
  set options(value: Array<ISelectOption>) {
    this._options = [...value];
    this.visibleOptions = [...value];
  }
  get options() {
    return this._options;
  }

  @ViewChild("inputRef", { static: false, read: ElementRef })
  searchInput: ElementRef;

  optionsDivMaxHeight = null;

  searchValue: string = this.selected
    ? this.selected.translate
      ? this.translate.instant(`${this.selected.text}`)
      : this.selected.text
    : "";

  isOpen = false;
  validationFailed = false;
  control: FormControl;

  onChange: (_: any) => void = () => {};
  onTouched: () => void = () => {};

  constructor(
    private translate: TranslateService,
    private injector: Injector
  ) {}

  ngAfterViewInit(): void {
    const ngControl: NgControl = this.injector.get(NgControl, null);
    if (ngControl) {
      this.control = ngControl.control as FormControl;
    }
  }

  ngOnChanges(changes: SimpleChanges) {
    const hasChanges = pipe(
      path(["options", "currentValue", "length"]),
      equals(path(["options", "previousValue", "length"])),
      not
    )(changes);

    if (hasChanges && isNotNilOrEmpty(this._value)) {
      this.writeValue(this._value);
    }
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  updateChanges() {
    this.onChange(this.selected.value);
  }

  writeValue(value: string | number): void {
    this._value = value;
    this.selected = this.options.find(option => option.value === `${value}`);
    if (this.searchable) {
      this.searchValue = this.selected
        ? this.selected.translate
          ? this.translate.instant(`${this.selected.text}`)
          : this.selected.text
        : "";
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  handleSelect(event: any, option: ISelectOption) {
    if (!option.disabled) {
      this.selected = option;
      this._value = this.selected.value;
      this.updateChanges();
      const { translate, text } = option;
      this.searchValue = translate
        ? this.translate.instant(`${text}`)
        : `${text}`;
    }

    this.isOpen = false;
    this.blurSelect();
    event.stopPropagation();
  }

  toggle() {
    this.isOpen = !this.isOpen;

    if (isNotNilOrEmpty(this.positionDiv)) {
      setTimeout(() => {
        if (this.isOpen) {
          this.optionsDivMaxHeight =
            this.positionDiv.nativeElement.offsetTop - 8;
        }
      }, 0);
    }

    if (!this.isOpen) {
      this.onTouched();
      this.blurSelect();
      if (this.selected) {
        const { translate, text } = this.selected;
        this.searchValue = translate
          ? this.translate.instant(`${text}`)
          : `${text}`;
      }
    } else {
      if (this.searchInput) {
        const { nativeElement } = this.searchInput;
        nativeElement.focus();
        if (nativeElement.setSelectionRange) {
          const cursorPlacement = this.searchValue.length * 2;
          nativeElement.setSelectionRange(cursorPlacement, cursorPlacement);
        }
      }
    }
  }

  getActive(option: ISelectOption) {
    return this.selected && option.value === this.selected.value;
  }

  handleClickOutside() {
    if (this.isOpen) {
      this.onTouched();
      if (this.selected) {
        const { translate, text } = this.selected;
        this.searchValue = translate
          ? this.translate.instant(`${text}`)
          : `${text}`;
      }
      this.isOpen = false;
      this.blurSelect();
    }
  }

  filterValues() {
    this.visibleOptions = this.options.filter(({ translate, text }) => {
      const target = translate ? this.translate.instant(`${text}`) : `${text}`;
      return target.toLowerCase().includes(this.searchValue.toLowerCase());
    });
  }

  getError(): boolean {
    if (this.control) {
      if (!this.showErrors) {
        return false;
      }
      const errors = (this.control && this.control.errors
        ? Object.entries(this.control.errors)
        : []
      ).filter(entry => entry[1]);
      if (!errors.length) {
        return false;
      }

      return true;
    }

    return false;
  }

  handleSearchFocus() {
    this.searchValue = "";
    this.filterValues();
  }

  blurSelect() {
    if (isNotNilOrEmpty(this.searchInput)) {
      const { nativeElement } = this.searchInput;
      nativeElement.blur();
    }
  }
}
