export interface ISelectOption {
  text: string | number;
  value: string | number;
  disabled?: boolean;
  checked?: boolean;
  translate?: boolean;
  children?: ISelectOption[];
}
