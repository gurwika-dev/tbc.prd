import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { LocalizationForChildModule } from "src/app/infrastructure/localization/localization.module";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { DirectivesModule } from "src/app/infrastructure/directives";
import { SelectComponent } from "./select.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DirectivesModule,
    LocalizationForChildModule,
    ReactiveFormsModule
  ],
  declarations: [SelectComponent],
  exports: [SelectComponent]
})
export class SelectModule {}
