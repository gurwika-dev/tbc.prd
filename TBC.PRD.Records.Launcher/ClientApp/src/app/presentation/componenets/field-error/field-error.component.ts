import { Component, OnInit, Input } from '@angular/core';
import { ControlContainer, FormGroupDirective, FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-field-error',
  templateUrl: './field-error.component.html',
  styleUrls: ['./field-error.component.scss'],
  viewProviders: [{ provide: ControlContainer, useExisting: FormGroupDirective }],
})
export class FieldErrorComponent implements OnInit {
  @Input() field: string;
  @Input() showErrors = false;
  @Input() errorMessages: { [error: string]: string } = {};

  // When this is true, the error component is hidden using "visibility: hidden" instead of display: none
  @Input() occupyEmptySpace = true;

  control: FormControl;

  constructor(private translateService: TranslateService, private parentForm: FormGroupDirective) {}

  ngOnInit() {
    const childForm = this.parentForm.form;
    this.control = <FormControl>childForm.controls[this.field];
  }

  getError(): string {
    if (!this.showErrors) {
      return null;
    }

    const errors = (this.control && this.control.errors ? Object.entries(this.control.errors) : []).filter(entry => entry[1]);
    if (!errors.length) {
      return null;
    }
    const error = errors[0][0];

    return this.errorMessages[error] || this.translateService.instant('validation.field-' + error);
  }
}
