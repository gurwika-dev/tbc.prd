import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { LocalizationForChildModule } from "src/app/infrastructure/localization/localization.module";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { TextInputComponent } from "./text-input.component";
import { DirectivesModule } from "src/app/infrastructure/directives";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DirectivesModule,
    LocalizationForChildModule,
    ReactiveFormsModule
  ],
  declarations: [TextInputComponent],
  exports: [TextInputComponent]
})
export class TextInputModule {}
