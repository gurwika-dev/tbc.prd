import {
  Component,
  forwardRef,
  Input,
  Injector,
  AfterViewInit,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy
} from "@angular/core";
import {
  ControlValueAccessor,
  NG_VALUE_ACCESSOR,
  NgControl,
  FormControl
} from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { fromEvent, Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { prop } from "ramda";
import {
  UnsubscribeOnDestroy,
  IUnsubscribeOnDestroy
} from "src/app/infrastructure/decorators/UnsubscribeOnDestroy";

const noop = () => {};

@UnsubscribeOnDestroy()
@Component({
  selector: "app-text-input",
  templateUrl: "./text-input.component.html",
  styleUrls: ["./text-input.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextInputComponent),
      multi: true
    }
  ]
})
export class TextInputComponent
  implements
    ControlValueAccessor,
    AfterViewInit,
    OnInit,
    OnDestroy,
    IUnsubscribeOnDestroy {
  @Input()
  value = "";

  @Input()
  autocomplete = "on";

  @Input()
  placeholder = "";

  @Input()
  label?: string = null;

  @Input()
  invalid?: boolean;

  @Input()
  errors?: Array<{ type: string; invalid: boolean; message: string }> = null;

  @Input()
  name?: string;

  @Input()
  type = "text";

  @Input()
  pattern = "";

  @Input()
  disabled = false;

  focused = false;

  @Input()
  showErrors = false;

  @Input()
  errorMessages: { [error: string]: string } = {};

  @Input()
  appGreaterThan?: number;

  @Input() minlength: number = null;
  @Input() maxlength: number = null;

  @Input() popoverText?: string = null;

  @ViewChild("ref", { static: false }) inputElement: ElementRef;

  destroy$: Subject<boolean>;

  onChange: (_: any) => void = noop;
  onTouched: () => void = noop;

  get nativeElement() {
    return this.inputElement.nativeElement;
  }

  private control: FormControl;

  constructor(
    private injector: Injector,
    private translateService: TranslateService
  ) {}

  ngAfterViewInit(): void {
    const ngControl: NgControl = this.injector.get(NgControl, null);
    if (ngControl) {
      this.control = ngControl.control as FormControl;
    }
  }

  ngOnInit() {
    if (this.type === "number") {
      fromEvent(this.inputElement.nativeElement, "mousewheel")
        .pipe(takeUntil(this.destroy$))
        .subscribe(() => {
          if (prop("type")(document.activeElement) === "number") {
            if ((document.activeElement as HTMLElement).blur) {
              (document.activeElement as HTMLElement).blur();
            }
          }
        });
    }
  }

  ngOnDestroy() {}

  onFocus() {
    this.focused = true;
  }

  onBlure() {
    this.focused = false;
    this.onTouched();
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
  }

  updateChanges() {
    this.onChange(this.value);
  }

  writeValue(value: any): void {
    this.value = value;
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }

  getError(): string {
    if (this.control) {
      if (!this.showErrors) {
        return null;
      }
      const errors = (this.control && this.control.errors
        ? Object.entries(this.control.errors)
        : []
      ).filter(entry => entry[1]);
      if (!errors.length) {
        return null;
      }
      const error = errors[0][0];

      return (
        this.errorMessages[error] ||
        this.translateService.instant("validation.field-" + error)
      );
    } else {
      // TODO: this must be removed after refactoring forms
      const errors = this.errors
        ? this.errors.filter(({ invalid }) => invalid).map(e => e.message)
        : [null];

      return errors[0];
    }
  }

  handleKeyup(event: any) {}

  handleKeypress({ key }) {
    if (this.type === "number") {
      const regex = /^([0-9]|\.)*$/;

      if (!key.match(regex)) {
        event.preventDefault();
      }
    }
  }

  handlePaste(event) {
    if (this.type === "number") {
      const value = event.clipboardData.getData("text/plain");

      const regex = /^([0-9]|\.)*$/;

      if (!value.match(regex)) {
        event.preventDefault();
      }
    }
  }
}
