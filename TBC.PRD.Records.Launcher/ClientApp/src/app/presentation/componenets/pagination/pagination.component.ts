import { Component, Input, Output, EventEmitter } from "@angular/core";
import { PaginationTypes } from "./pagination-type.enum";
import { IPagination } from "./pagination.model";

@Component({
  selector: "app-pagination",
  templateUrl: "./pagination.component.html",
  styleUrls: ["./pagination.component.scss"]
})
export class PaginationComponent {
  public paginationTypes = PaginationTypes;

  @Input()
  public listPagination: Array<IPagination>;

  @Output()
  public change = new EventEmitter<number>();

  changePage(listPage: IPagination): void {
    this.change.emit(listPage.index);
  }
}
