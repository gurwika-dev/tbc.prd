import { PaginationTypes } from "./pagination-type.enum";

export interface IPagination {
  type: PaginationTypes;
  index?: number;
}

export interface IPaginationConfig {
  limit: number;
  totalItems?: number;
}
