import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { LocalizationForChildModule } from "src/app/infrastructure/localization/localization.module";
import { PaginationComponent } from "./pagination.component";

@NgModule({
  imports: [CommonModule, RouterModule, LocalizationForChildModule],
  declarations: [PaginationComponent],
  exports: [PaginationComponent]
})
export class PaginationModule {}
