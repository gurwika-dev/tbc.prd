import { Component, Input, Output, EventEmitter } from "@angular/core";
import {
  UnsubscribeOnDestroy,
  IUnsubscribeOnDestroy
} from "src/app/infrastructure/decorators/UnsubscribeOnDestroy";
import { Subject } from "rxjs";
import { NavigationCondition } from "src/app/infrastructure/store/ui/models/navigation-condition";
import { AppState } from "src/app/infrastructure/store/app-state.model";
import { Store, select } from "@ngrx/store";
import { UIAction } from "src/app/infrastructure/store/ui";
import { isNotNilOrEmpty } from "src/app/infrastructure/utils/is-or-not/isNotNilOrEmpty";
import { filter, takeUntil } from "rxjs/operators";

@UnsubscribeOnDestroy()
@Component({
  selector: "app-navigation",
  templateUrl: "./navigation.component.html",
  styleUrls: ["./navigation.component.scss"]
})
export class NavigationComponent implements IUnsubscribeOnDestroy {
  public destroy$: Subject<boolean>;
  public navigationCondition: NavigationCondition;

  constructor(
    private readonly store: Store<AppState>,
    private readonly uIAction: UIAction
  ) {}

  ngOnInit(): void {
    this.subscribeNavigationState();
  }

  ngOnDestroy() {}

  handleHideNavigation() {
    this.store.dispatch(this.uIAction.hideNavigation());
  }

  subscribeNavigationState() {
    this.store
      .pipe(
        select(store => store.ui.navigation),
        takeUntil(this.destroy$),
        filter(isNotNilOrEmpty)
      )
      .subscribe(state => {
        this.navigationCondition = state;
      });
  }
}
