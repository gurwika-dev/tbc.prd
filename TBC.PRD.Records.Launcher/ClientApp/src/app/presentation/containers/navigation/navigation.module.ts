import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { NavigationComponent } from "./navigation.component";
import { RouterModule } from "@angular/router";
import { LocalizationForChildModule } from "src/app/infrastructure/localization/localization.module";

@NgModule({
  imports: [CommonModule, RouterModule, LocalizationForChildModule],
  declarations: [NavigationComponent],
  exports: [NavigationComponent]
})
export class NavigationModule {}
