import { Component, Input, Output } from "@angular/core";
import { Store } from "@ngrx/store";
import { AppState } from "src/app/infrastructure/store/app-state.model";
import { UIAction } from "src/app/infrastructure/store/ui";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent {
  @Input() backLink: string;
  @Input() title: string;

  constructor(
    private readonly store: Store<AppState>,
    private readonly uIAction: UIAction
  ) {}

  handleShowNavigation() {
    this.store.dispatch(this.uIAction.showNavigation());
  }
}
