import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { LocalizationForChildModule } from "src/app/infrastructure/localization/localization.module";
import { BoxComponent } from "./box.component";

@NgModule({
  imports: [CommonModule, RouterModule, LocalizationForChildModule],
  declarations: [BoxComponent],
  exports: [BoxComponent]
})
export class BoxModule {}
