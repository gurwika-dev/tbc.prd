import { Component, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-box",
  templateUrl: "./box.component.html",
  styleUrls: ["./box.component.scss"]
})
export class BoxComponent {
  @Input() heading = null;
  @Input() urlText = null;
  @Input() url = null;

  constructor() {}
}
