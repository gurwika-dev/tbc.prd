import { CommonModule } from "@angular/common";
import { NgModule, Input } from "@angular/core";
import { RouterModule } from "@angular/router";
import { LocalizationForChildModule } from "src/app/infrastructure/localization/localization.module";
import { DataTableComponent } from "./data-table.component";

@NgModule({
  imports: [CommonModule, RouterModule, LocalizationForChildModule],
  declarations: [DataTableComponent],
  exports: [DataTableComponent]
})
export class DataTableModule {}
