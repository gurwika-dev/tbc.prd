import {
  Component,
  Input,
  Output,
  EventEmitter,
  TemplateRef
} from "@angular/core";
import { PaginationService } from "src/app/application/services/pagination.service";

@Component({
  selector: "app-data-table",
  templateUrl: "./data-table.component.html",
  styleUrls: ["./data-table.component.scss"]
})
export class DataTableComponent {
  @Input() public headers: string[];
  @Input() public items: Array<any> = [];
  @Input() public $item: TemplateRef<any>;

  @Input() public limit: number;
  @Input() public page: number;
  @Input() public count: number;
}
