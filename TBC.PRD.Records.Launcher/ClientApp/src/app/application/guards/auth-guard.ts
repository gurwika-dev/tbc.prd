import { Injectable } from "@angular/core";
import { CanActivate, CanLoad, Router } from "@angular/router";
import { Observable } from "rxjs";
import { AuthService } from "../services/auth.service";
import { map } from "rxjs/operators";

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {
  constructor(
    private readonly router: Router,
    private readonly authService: AuthService
  ) {}

  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkAuthentication();
  }

  canLoad(): Observable<boolean> | Promise<boolean> | boolean {
    return this.checkAuthentication();
  }

  checkAuthentication(): Observable<boolean> | Promise<boolean> | boolean {
    return this.authService.isAuthenticated().pipe(
      map(next => {
        if (next) {
          return true;
        } else {
          this.router.navigate(["/identity"]);
          return false;
        }
      })
    );
  }
}
