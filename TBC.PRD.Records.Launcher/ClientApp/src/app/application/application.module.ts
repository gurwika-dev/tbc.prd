import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { CookieService as CookieClient } from "ngx-cookie-service";

import { AuthService } from "./services/auth.service";
import { HttpInterceptor } from "./interceptor/token-interceptor";
import { AuthGuard } from "./guards/auth-guard";
import { CookieService } from "./services/cookie.service";
import { LanguageService } from "./services/language.service";
import { PaginationService } from "./services/pagination.service";

@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule, BrowserAnimationsModule],
  exports: [CommonModule, HttpClientModule, BrowserAnimationsModule],
  providers: [
    AuthGuard,
    AuthService,
    CookieClient,
    CookieService,
    LanguageService,
    PaginationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptor,
      multi: true
    }
  ]
})
export class ApplicationModule {}
