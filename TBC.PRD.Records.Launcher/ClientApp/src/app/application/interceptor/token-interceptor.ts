import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpResponse,
  HttpErrorResponse
} from "@angular/common/http";

import { Observable, throwError } from "rxjs";
import { map, filter, tap, catchError } from "rxjs/operators";

import { environment } from "src/environments/environment";
import { AuthService } from "../services/auth.service";
import { isNotNil } from "src/app/infrastructure/utils/is-or-not/isNotNil";
import { Router } from "@angular/router";
import { LanguageService } from "../services/language.service";

@Injectable()
export class HttpInterceptor implements HttpInterceptor {
  baseUrl = environment.apiBaseUrl;

  constructor(
    private readonly authService: AuthService,
    private readonly languageService: LanguageService,
    private readonly router: Router
  ) {}

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    req.clone({
      setHeaders: {
        "Accept-Language": `${this.languageService.selected}`
      }
    });

    if (
      !req.url.includes("http") &&
      !req.url.includes("https") &&
      !req.url.includes("assets")
    ) {
      req = req.clone({ url: `${this.baseUrl}${req.url}` });

      this.authService
        .getAccessToken()
        .pipe(filter(isNotNil))
        .subscribe(accessToken => {
          req = req.clone({
            setHeaders: {
              Authorization: `Bearer ${accessToken}`
            }
          });
        });
    }

    return next.handle(req).pipe(
      map(event => {
        if (event instanceof HttpResponse) {
          event = event.clone({ body: event.body });
        }
        return event;
      }),
      catchError(err => {
        if (err instanceof HttpErrorResponse && err.status === 401) {
          this.authService.signOut().subscribe(() => {
            this.router.navigate(["identity"]);
          });
        }

        return throwError({
          type: err.status,
          data: err
        });
      })
    );
  }
}
