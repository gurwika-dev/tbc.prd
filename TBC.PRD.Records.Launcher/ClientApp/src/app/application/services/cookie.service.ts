import { Injectable } from "@angular/core";
import { CookieService as CookieClient } from "ngx-cookie-service";
import { isNilOrEmpty } from "src/app/infrastructure/utils/is-or-not/isNilOrEmpty";

@Injectable()
export class CookieService {
  constructor(private cookieClient: CookieClient) {}

  public get(key: string, fallback?: string): string {
    if (!fallback) {
      fallback = "";
    }

    const cookieValue = this.cookieClient.get(key);

    if (typeof cookieValue === "undefined" || isNilOrEmpty(cookieValue)) {
      return fallback;
    }

    return cookieValue;
  }

  public set(key: string, value: string, days?: number) {
    if (!days) {
      days = 365 + 1;
    }
    const d = new Date();
    d.setTime(d.getTime() + days * 24 * 60 * 60 * 1000);

    this.cookieClient.set(key, value, d, "/");
  }

  public delete(key: string) {
    this.cookieClient.delete(key, "/");
  }
}
