import { Injectable } from "@angular/core";
import { IPagination } from "src/app/presentation/componenets/pagination/pagination.model";
import { isNilOrEmpty } from "src/app/infrastructure/utils/is-or-not/isNilOrEmpty";
import { PaginationTypes } from "src/app/presentation/componenets/pagination/pagination-type.enum";

@Injectable()
export class PaginationService {
  constructor() {}

  getPagination(
    page: number,
    totalItems: number,
    limit: number
  ): Array<IPagination> {
    const pagination: Array<IPagination> = [];
    const adjacents = 1;
    let counter: number;

    /* Setup page vars for display. */
    if (isNilOrEmpty(page) || page === 0) {
      page = 1;
    }

    const prev = page - 1;
    const next = page + 1;
    const lastpage = Math.ceil(totalItems / limit);
    const lpm1 = lastpage - 1;

    if (lastpage < 2) {
      return pagination;
    }

    if (page > 1) {
      pagination.push({ type: PaginationTypes.Prev, index: prev });
    }

    /* pages */
    if (lastpage < 7 + adjacents * 2) {
      for (counter = 1; counter <= lastpage; counter++) {
        if (counter === page) {
          pagination.push({
            type: PaginationTypes.Active,
            index: counter
          });
        } else {
          pagination.push({
            type: PaginationTypes.Page,
            index: counter
          });
        }
      }
    } else {
      /*close to beginning; only hide later pages*/
      if (page <= 1 + adjacents * 2) {
        for (counter = 1; counter < 4 + adjacents * 2; counter++) {
          if (counter === page) {
            pagination.push({
              type: PaginationTypes.Active,
              index: counter
            });
          } else {
            pagination.push({
              type: PaginationTypes.Page,
              index: counter
            });
          }
        }

        pagination.push({ type: PaginationTypes.Dots });
        pagination.push({ type: PaginationTypes.Page, index: lpm1 });
        pagination.push({ type: PaginationTypes.Page, index: lastpage });
      } else if (lastpage - adjacents * 2 > page && page > adjacents * 2) {
        /*in middle; hide some front and some back*/
        pagination.push({ type: PaginationTypes.Page, index: 1 });
        // pagination.push({ type: PaginationTypes.Page, index: 2 });
        pagination.push({ type: PaginationTypes.Dots });

        for (
          counter = page - adjacents;
          counter <= page + adjacents;
          counter++
        ) {
          if (counter === page) {
            pagination.push({
              type: PaginationTypes.Active,
              index: counter
            });
          } else {
            pagination.push({
              type: PaginationTypes.Page,
              index: counter
            });
          }
        }

        pagination.push({ type: PaginationTypes.Dots });
        // pagination.push({ type: PaginationTypes.Page, index: lpm1 });
        pagination.push({ type: PaginationTypes.Page, index: lastpage });
      } else {
        /*close to end; only hide early pages*/
        pagination.push({ type: PaginationTypes.Page, index: 1 });
        pagination.push({ type: PaginationTypes.Page, index: 2 });
        pagination.push({ type: PaginationTypes.Dots });

        for (
          counter = lastpage - (2 + adjacents * 2);
          counter <= lastpage;
          counter++
        ) {
          if (counter === page) {
            pagination.push({
              type: PaginationTypes.Active,
              index: counter
            });
          } else {
            pagination.push({
              type: PaginationTypes.Page,
              index: counter
            });
          }
        }
      }
    }

    if (page < counter - 1) {
      pagination.push({ type: PaginationTypes.Next, index: next });
    }

    return pagination;
  }
}
