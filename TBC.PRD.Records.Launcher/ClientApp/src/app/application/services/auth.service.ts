import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { CookieService } from "./cookie.service";
import { isNotNilOrEmpty } from "src/app/infrastructure/utils/is-or-not/isNotNilOrEmpty";

@Injectable()
export class AuthService {
  public accessToken: BehaviorSubject<string>;
  public authenticationState: BehaviorSubject<boolean>;

  constructor(private readonly cookieService: CookieService) {
    this.initState();
  }

  private initState() {
    this.accessToken = new BehaviorSubject<string>(
      this.cookieService.get("accessToken")
    );
    this.authenticationState = new BehaviorSubject<boolean>(
      isNotNilOrEmpty(this.cookieService.get("accessToken"))
    );
  }

  getAccessToken(): Observable<string> {
    return this.accessToken;
  }

  isAuthenticated(): Observable<boolean> {
    return this.authenticationState;
  }

  setState(accessToken: string) {
    this.cookieService.set("accessToken", accessToken);
    this.accessToken.next(accessToken);
    this.authenticationState.next(true);
  }

  signOut() {
    return new Observable(observer => {
      this.cookieService.delete("accessToken");
      this.accessToken.next(null);
      this.authenticationState.next(false);
      observer.next();
      observer.complete();
    });
  }
}
