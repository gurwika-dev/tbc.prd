import { Injectable } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";

@Injectable({
  providedIn: "root"
})
export class LanguageService {
  public selected = "en";

  constructor(private translate: TranslateService) {}

  init() {
    this.translate.setDefaultLang(this.selected);
    this.translate.use(this.selected);
  }
}
