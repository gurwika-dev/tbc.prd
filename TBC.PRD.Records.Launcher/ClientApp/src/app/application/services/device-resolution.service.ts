import { Injectable } from '@angular/core';
import { not } from 'ramda';

@Injectable({
  providedIn: 'root',
})
export class DeviceResolutionService {
  constructor() {}

  get isMobile(): boolean {
    return window.innerWidth < 768;
  }

  get isTablet(): boolean {
    return not(this.isMobile) && window.innerWidth < 1024;
  }

  get isSmallDesktop(): boolean {
    return not(this.isTablet) && not(this.isMobile) && window.innerWidth < 1366;
  }

  get isDesktop(): boolean {
    return not(this.isTablet) && not(this.isMobile) && not(this.isSmallDesktop);
  }
}
