import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { AuthGuard } from "./application/guards/auth-guard";

const routes: Routes = [
  {
    path: "",
    loadChildren: () =>
      import("./presentation/modules/dashboard/dashboard.module").then(
        m => m.DashboardModule
      ),
    canActivate: [AuthGuard]
  },
  {
    path: "identity",
    loadChildren: () =>
      import("./presentation/modules/identity/identity.module").then(
        m => m.IdentityModule
      )
  },
  {
    path: "404",
    loadChildren: () =>
      import("./presentation/modules/errors/errors.module").then(
        m => m.ErrorsModule
      )
  },
  {
    path: "**",
    redirectTo: "404"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
