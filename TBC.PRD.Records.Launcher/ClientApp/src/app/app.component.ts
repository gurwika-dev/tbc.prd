import { Component } from "@angular/core";
import { LanguageService } from "./application/services/language.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html"
})
export class AppComponent {
  constructor(private readonly languageService: LanguageService) {
    this.initApp();
  }

  initApp() {
    this.languageService.init();
  }
}
