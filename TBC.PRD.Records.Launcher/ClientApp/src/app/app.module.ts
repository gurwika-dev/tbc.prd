import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app.router.module";
import { LocalizationForRootModule } from "./infrastructure/localization/localization.module";
import { ApplicationModule } from "./application/application.module";
import { AppStoreModule } from "./infrastructure/store/app-store.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule.withServerTransition({ appId: "ng-cli-universal" }),
    ApplicationModule,
    FormsModule,
    AppRoutingModule,
    LocalizationForRootModule,
    AppStoreModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
