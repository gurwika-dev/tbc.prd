// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  identity: {
    apiurl: "https://localhost:44381",
    clientId: "TBC.PRD.Api",
    clientSercret: "API.Secret"
  },
  apiBaseUrl: "https://localhost:44377/api",
  cdnBaseUrl: "https://localhost:44377/uploads",
  validator: {
    maxPhoneNumber: 50,
    minPhoneNumber: 4
  }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
