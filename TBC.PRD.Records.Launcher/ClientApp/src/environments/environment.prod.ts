export const environment = {
  production: false,
  identity: {
    apiurl: "https://localhost:44381",
    clientId: "TBC.PRD.Api",
    clientSercret: "API.Secret"
  },
  apiBaseUrl: "https://localhost:44377/api",
  cdnBaseUrl: "https://localhost:44377/uploads",
  validator: {
    maxPhoneNumber: 50,
    minPhoneNumber: 4
  }
};
