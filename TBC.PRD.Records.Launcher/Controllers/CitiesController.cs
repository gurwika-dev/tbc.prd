﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TBC.PRD.Records.Application.Cities.Queries.GetCitiesList;

namespace TBC.PRD.Records.Launcher.Controllers
{
    [Route("api/cities")]
    public class CitiesController : BaseController
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<GetCitiesListVM>> GetAll([FromQuery] GetCitiesListModel model)
        {
            return Ok(await Mediator.Send(new GetCitiesListQuery { Model = model }));
        }
    }
}