﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TBC.PRD.Records.Application.Persons.Queries.GetStatistics;
using TBC.PRD.Records.Application.Records.Queries.GetPersonById;
using TBC.PRD.Records.Application.Persons.Queries.GetPersonsList;
using TBC.PRD.Records.Application.Persons.Commands.RemovePerson;
using TBC.PRD.Records.Application.Persons.Commands.CreatePerson;
using TBC.PRD.Records.Application.Persons.Commands.UpdatePerson;
using TBC.PRD.Records.Application.Persons.Commands.UploadPersonThumbnail;

namespace TBC.PRD.Records.Launcher.Controllers
{
    [Route("api/persons")]
    public class PersonController : BaseController
    {
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<GetPersonsListVM>> GetAll([FromQuery] GetPersonsListModel model)
        {
            return Ok(await Mediator.Send(new GetPersonsListQuery { Model = model }));
        }

        [HttpGet("{personId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetPersonByIdVM>> Get(Guid personId)
        {
            return Ok(await Mediator.Send(new GetPersonByIdQuery { PersonId = personId }));
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Create([FromBody]CreatePersonModel model)
        {
            await Mediator.Send(new CreatePersonCommand { Model = model  });

            return NoContent();
        }

        [HttpPut("{personId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Update(Guid personId, [FromBody]UpdatePersonModel model)
        {
            await Mediator.Send(new UpdatePersonCommand { PersonId = personId, Model = model });

            return NoContent();
        }

        [HttpDelete("{personId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid personId)
        {
            await Mediator.Send(new RemovePersonCommand { PersonId = personId });

            return NoContent();
        }

        [HttpPost("{personId}/thumbnail")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Upload(Guid personId, [FromForm]UploadPersonThumbnailModel model)
        {
            await Mediator.Send(new UploadPersonThumbnailCommand { PersonId = personId, Model = model });

            return NoContent();
        }

        [HttpGet("{personId}/statistics")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetStatisticsVM>> Statistics(Guid personId)
        {
            return Ok(await Mediator.Send(new GetStatisticsQuery { PersonId = personId }));
        }
    }
}