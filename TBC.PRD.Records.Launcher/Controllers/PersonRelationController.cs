﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TBC.PRD.Records.Application.PersonRelations.Commands.CreatePersonsRelation;
using TBC.PRD.Records.Application.PersonRelations.Commands.RemovePersonsRelation;
using TBC.PRD.Records.Application.PersonRelations.Queries.GetPersonRelationsList;

namespace TBC.PRD.Records.Launcher.Controllers
{
    [Route("api/persons")]
    public class PersonRelationController : BaseController
    {
        [HttpGet("{personId}/relations")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult<GetPersonRelationsListVM>> GetAll(Guid personId, [FromQuery] GetPersonRelationsListModel model)
        {
            return Ok(await Mediator.Send(new GetPersonRelationsListQuery { PersonId = personId, Model = model }));
        }

        [HttpPost("{personId}/relations")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Create(Guid personId, [FromBody]CreatePersonsRelationModel model)
        {
            await Mediator.Send(new CreatePersonsRelationCommand { PersonId = personId, Model = model });

            return NoContent();
        }

        [HttpDelete("{personId}/relations/{personRelationId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(Guid personId, Guid personRelationId)
        {
            await Mediator.Send(new RemovePersonsRelationCommand{ PersonId = personId, PersonRelationId = personRelationId });

            return NoContent();
        }
    }
}