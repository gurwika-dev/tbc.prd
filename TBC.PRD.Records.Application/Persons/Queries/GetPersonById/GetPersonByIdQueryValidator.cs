﻿using FluentValidation;

namespace TBC.PRD.Records.Application.Records.Queries.GetPersonById
{
    public class GetPersonByIdQueryValidator : AbstractValidator<GetPersonByIdQuery>
    {
        public GetPersonByIdQueryValidator()
        {
            RuleFor(x => x.PersonId).NotNull();
        }
    }
}
