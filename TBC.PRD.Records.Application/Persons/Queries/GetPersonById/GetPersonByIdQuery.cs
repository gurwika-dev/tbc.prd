﻿using System;
using System.Collections.Generic;
using System.Text;
using TBC.PRD.Common.Domain.Common.Abstraction.Queries;

namespace TBC.PRD.Records.Application.Records.Queries.GetPersonById
{
    public class GetPersonByIdQuery : IQuery<GetPersonByIdVM>
    {
        public Guid PersonId { get; set; }
    }
}
