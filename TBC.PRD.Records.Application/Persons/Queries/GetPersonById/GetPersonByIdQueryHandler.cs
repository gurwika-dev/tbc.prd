﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Common.Abstraction.Queries;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Domain.Exceptions;

namespace TBC.PRD.Records.Application.Records.Queries.GetPersonById
{
    public class GetPersonByIdQueryHandler : IQueryHandler<GetPersonByIdQuery, GetPersonByIdVM>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetPersonByIdQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GetPersonByIdVM> Handle(GetPersonByIdQuery request, CancellationToken cancellationToken)
        {
            var entry = _context.Set<Person>()
                                    .ProjectTo<GetPersonByIdDTO>(_mapper.ConfigurationProvider)
                                        .SingleOrDefault(e => e.Id == request.PersonId);

            if (entry == null)
            {
                throw new DomainException("Person does not exists");
            }

            return await Task.Run(() =>
             {
                 return new GetPersonByIdVM
                 {
                     Entity = entry
                 };
             });
        }
    }
}
