﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TBC.PRD.Records.Application.Records.Queries.GetPersonById
{
    public class GetPersonByIdVM
    {
        public GetPersonByIdDTO Entity { get; set; }
    }
}
