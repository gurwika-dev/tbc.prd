﻿using System;
using TBC.PRD.Common.Domain.Common.Abstraction.Queries;

namespace TBC.PRD.Records.Application.Persons.Queries.GetStatistics
{
    public class GetStatisticsQuery : IQuery<GetStatisticsVM>
    {
        public Guid PersonId { get; set; }
    }
}
