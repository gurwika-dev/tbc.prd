﻿using FluentValidation;

namespace TBC.PRD.Records.Application.Persons.Queries.GetStatistics
{
    public class GetStatisticsQueryValidator : AbstractValidator<GetStatisticsQuery>
    {
        public GetStatisticsQueryValidator()
        {
            RuleFor(x => x.PersonId).NotNull();
        }
    }
}
