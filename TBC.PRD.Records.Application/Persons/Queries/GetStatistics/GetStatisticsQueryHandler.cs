﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Common.Abstraction.Queries;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Domain.Enumarations.Records;
using TBC.PRD.Common.Domain.Exceptions;
using TBC.PRD.Records.Application.Common.Types;

namespace TBC.PRD.Records.Application.Persons.Queries.GetStatistics
{
    public class GetStatisticsQueryHandler : IQueryHandler<GetStatisticsQuery, GetStatisticsVM>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetStatisticsQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GetStatisticsVM> Handle(GetStatisticsQuery request, CancellationToken cancellationToken)
        {
            var person = _context.Set<Person>().SingleOrDefault(e => e.Id == request.PersonId);

            if (person == null)
            {
                throw new DomainException("Person does not exists");
            }

            var entries = new List<GetStatisticsDTO>(); 
            var values = Enum.GetValues(typeof(RelationType));

            foreach (RelationType type in values)
            {
                var counter = await _context.Set<PersonRelation>().CountAsync(e => e.Type == type && e.PersonId == request.PersonId);
                entries.Add(new GetStatisticsDTO { Counter = counter, Type = type, Category = PersonRelationCategory.DIRECT });

                counter = await _context.Set<PersonRelation>().CountAsync(e => e.Type == type && e.RelatedPersonId == request.PersonId);
                entries.Add(new GetStatisticsDTO { Counter = counter, Type = type, Category = PersonRelationCategory.INDIRECT });
            }

            return new GetStatisticsVM
            {
                Entries = entries
            };
        }
    }
}
