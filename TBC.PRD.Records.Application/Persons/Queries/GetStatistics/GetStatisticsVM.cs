﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TBC.PRD.Records.Application.Persons.Queries.GetStatistics
{
    public class GetStatisticsVM
    {
        public IList<GetStatisticsDTO> Entries { get; set; }
    }
}
