﻿using TBC.PRD.Common.Domain.Enumarations.Records;
using TBC.PRD.Records.Application.Common.Types;

namespace TBC.PRD.Records.Application.Persons.Queries.GetStatistics
{
    public class GetStatisticsDTO
    {
        public int Counter { get; set; }
        public RelationType Type { get; set; }
        public PersonRelationCategory Category { get; set; }
    }
}
