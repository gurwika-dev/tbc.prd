﻿using AutoMapper;
using System;
using TBC.PRD.Common.Application.Mappings.Abstraction;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Domain.Enumarations.Application;

namespace TBC.PRD.Records.Application.Persons.Queries.GetPersonsList
{
    public class GetPersonsListDTO : IMapFrom<Person>
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public GenderType GenderType { get; set; }
        public string PersonalNumber { get; set; }
        public string PhoneNumber { get; set; }
        public Guid cityId { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<Person, GetPersonsListDTO>();
        }
    }
}
