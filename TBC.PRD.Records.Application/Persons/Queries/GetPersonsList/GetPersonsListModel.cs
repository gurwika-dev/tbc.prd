﻿using System;
using TBC.PRD.Common.Domain.Enumarations.Application;

namespace TBC.PRD.Records.Application.Persons.Queries.GetPersonsList
{
    public class GetPersonsListModel
    {
        public int Limit { get; set; }
        public int Page { get; set; }
        public string KeyWord { get; set; }
        public bool? IsAdvancedSearch { get; set; }
        public DateTime? StartBirthDate { get; set; }
        public DateTime? EndBirthDate { get; set; }
        public GenderType? GenderType { get; set; }
    }
}
