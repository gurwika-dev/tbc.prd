﻿using FluentValidation;

namespace TBC.PRD.Records.Application.Persons.Queries.GetPersonsList
{
    public class GetPersonsListQueryValidator : AbstractValidator<GetPersonsListQuery>
    {
        public GetPersonsListQueryValidator()
        {
            RuleFor(x => x.Model.Page).GreaterThanOrEqualTo(0).NotNull();
            RuleFor(x => x.Model.Limit).GreaterThan(1).NotNull();
            RuleFor(x => x.Model.KeyWord).MaximumLength(50);
            RuleFor(x => x.Model.GenderType).IsInEnum();
        }
    }
}