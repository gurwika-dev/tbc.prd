﻿using System;
using System.Collections.Generic;
using System.Text;
using TBC.PRD.Common.Domain.Common.Abstraction.Queries;

namespace TBC.PRD.Records.Application.Persons.Queries.GetPersonsList
{
    public class GetPersonsListQuery : IQuery<GetPersonsListVM>
    {
        public GetPersonsListModel Model { get; set; }
    }
}
