﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Common.Abstraction.Queries;
using TBC.PRD.Common.Domain.Entities.Records;

namespace TBC.PRD.Records.Application.Persons.Queries.GetPersonsList
{
    public class GetPersonsListQueryHandler : IQueryHandler<GetPersonsListQuery, GetPersonsListVM>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetPersonsListQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GetPersonsListVM> Handle(GetPersonsListQuery request, CancellationToken cancellationToken)
        {
            var entriesQuery = _context.Set<Person>().Where(x => true);

            if (!string.IsNullOrEmpty(request.Model.KeyWord))
            {
                if (request.Model.IsAdvancedSearch.HasValue && request.Model.IsAdvancedSearch.Value)
                {
                    entriesQuery = entriesQuery.Include(e => e.City)
                                .Where(e => e.Id.ToString() == request.Model.KeyWord || 
                                                e.FirstName.Contains(request.Model.KeyWord) ||
                                                e.LastName.Contains(request.Model.KeyWord) ||
                                                e.PersonalNumber.Contains(request.Model.KeyWord) ||
                                                e.PhoneNumber.Contains(request.Model.KeyWord) ||
                                                e.City.DisplayName.Contains(request.Model.KeyWord)
                                        );
                }
                else
                {
                    entriesQuery = entriesQuery.Where(e => e.FirstName.Contains(request.Model.KeyWord) ||
                                                    e.LastName.Contains(request.Model.KeyWord) ||
                                                e.PersonalNumber.Contains(request.Model.KeyWord)
                                            );
                }
            }

            if (request.Model.GenderType.HasValue)
            {
                entriesQuery = entriesQuery.Where(e => e.GenderType == request.Model.GenderType.Value);
            }

            if (request.Model.IsAdvancedSearch.HasValue && request.Model.IsAdvancedSearch.Value)
            {

                if (request.Model.StartBirthDate.HasValue)
                {
                    entriesQuery = entriesQuery.Where(e => e.BirthDate >= request.Model.StartBirthDate.Value);
                }

                if (request.Model.EndBirthDate.HasValue)
                {
                    entriesQuery = entriesQuery.Where(e => e.BirthDate <= request.Model.EndBirthDate.Value);
                }
            }


            var count = entriesQuery.Count();
            var entries = await entriesQuery
                                    .ProjectTo<GetPersonsListDTO>(_mapper.ConfigurationProvider)
                                    .Skip(request.Model.Limit * (request.Model.Page - 1))
                                    .Take(request.Model.Limit)
                                    .ToListAsync(cancellationToken);

            return new GetPersonsListVM
            {
                Entries = entries,
                Count = count
            };
        }
    }
}
