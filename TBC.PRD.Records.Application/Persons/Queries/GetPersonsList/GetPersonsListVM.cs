﻿using System.Collections.Generic;

namespace TBC.PRD.Records.Application.Persons.Queries.GetPersonsList
{
    public class GetPersonsListVM
    {
        public IList<GetPersonsListDTO> Entries { get; set; }
        public int Count { get; set; }
    }
}
