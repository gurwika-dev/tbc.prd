﻿using MediatR;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using TBC.PRD.Common.Application.Common.Abstraction;
using TBC.PRD.Common.Application.Common.Handlers;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Common.Abstraction.Commands;

namespace TBC.PRD.Records.Application.Persons.Commands.UploadPersonThumbnail
{
    public class UploadPersonThumbnailCommandHandler : CommandHandler, ICommandHandler<UploadPersonThumbnailCommand>
    {
        private readonly IWebHostEnvironment _environment;

        public UploadPersonThumbnailCommandHandler(IWebHostEnvironment environment, IApplicationDbContext context, IEventBus eventBus) : base(context, eventBus)
        {
            _environment = environment;
        }

        public async Task<Unit> Handle(UploadPersonThumbnailCommand request, CancellationToken cancellationToken)
        {
            var extension = Path.GetExtension(request.Model.Photo.FileName);
            var uploads = Path.Combine(_environment.WebRootPath, "uploads", request.PersonId.ToString());
            var filePath = Path.Combine(uploads, request.Model.FileId.ToString() + extension);

            if (!Directory.Exists(uploads))
            {
                Directory.CreateDirectory(uploads);
            }

            request.Model.Photo.CopyTo(new FileStream(filePath, FileMode.Create));

            return await Task.Run(() =>
            {
                return Unit.Value;
            });
        }
    }
}
