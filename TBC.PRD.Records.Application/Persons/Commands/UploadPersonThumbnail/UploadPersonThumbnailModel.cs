﻿using Microsoft.AspNetCore.Http;
using System;

namespace TBC.PRD.Records.Application.Persons.Commands.UploadPersonThumbnail
{
    public class UploadPersonThumbnailModel
    {
        public Guid FileId { get; set; }
        public IFormFile Photo { get; set; }
    }
}
