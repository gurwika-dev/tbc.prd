﻿using System;
using TBC.PRD.Common.Domain.Common.Abstraction.Commands;

namespace TBC.PRD.Records.Application.Persons.Commands.UploadPersonThumbnail
{
    public class UploadPersonThumbnailCommand : ICommand
    {
        public Guid PersonId { get; set; }
        public UploadPersonThumbnailModel Model { get; set; }
    }
}
