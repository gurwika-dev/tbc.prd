﻿using FluentValidation;

namespace TBC.PRD.Records.Application.Persons.Commands.UploadPersonThumbnail
{
    public class UploadPersonThumbnailCommandValidator : AbstractValidator<UploadPersonThumbnailCommand>
    {
        public UploadPersonThumbnailCommandValidator()
        {
            RuleFor(x => x.PersonId).NotNull();
            RuleFor(x => x.Model.FileId).NotNull();
            RuleFor(x => x.Model.Photo).NotNull();
        }
    }
}
