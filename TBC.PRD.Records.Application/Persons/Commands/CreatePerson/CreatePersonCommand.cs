﻿using TBC.PRD.Common.Domain.Common.Abstraction.Commands;

namespace TBC.PRD.Records.Application.Persons.Commands.CreatePerson
{
    public class CreatePersonCommand : ICommand
    {
        public CreatePersonModel Model { get; set; }
    }
}
