﻿using System;
using TBC.PRD.Common.Domain.Enumarations.Application;

namespace TBC.PRD.Records.Application.Persons.Commands.UpdatePerson
{
    public class UpdatePersonModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public GenderType GenderType { get; set; }
        public string PersonalNumber { get; set; }
        public DateTime BirthDate { get; set; }
        public string PhoneNumber { get; set; }
        public string ThumbnailUrl { get; set; }
        public Guid CityId { get; set; }
    }
}
