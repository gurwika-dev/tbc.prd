﻿using FluentValidation;
using System;

namespace TBC.PRD.Records.Application.Persons.Commands.UpdatePerson
{
    public class UpdatePersonCommandValidator : AbstractValidator<UpdatePersonCommand>
    {
        public UpdatePersonCommandValidator()
        {
            RuleFor(x => x.Model.FirstName).MinimumLength(2).MaximumLength(50).Matches("^([a-zA-Z]+|[ა-ჰ]+)$").NotNull();
            RuleFor(x => x.Model.LastName).MinimumLength(2).MaximumLength(50).Matches("^([a-zA-Z]+|[ა-ჰ]+)$").NotNull();
            RuleFor(x => x.Model.GenderType).NotNull().IsInEnum().NotNull();

            RuleFor(x => x.Model.PersonalNumber).Length(11).Matches("^([0-9]{11,11})$").NotNull();

            RuleFor(x => x.Model.BirthDate).NotNull().Must(BeAValidDate).NotNull();

            RuleFor(x => x.Model.ThumbnailUrl).NotNull();
            RuleFor(x => x.Model.CityId).NotNull();
        }

        private bool BeAValidDate(DateTime birthDate)
        {
            return birthDate < DateTime.Now.AddYears(-18);
        }
    }
}
