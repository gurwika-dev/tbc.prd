﻿using System;
using TBC.PRD.Common.Domain.Common.Abstraction.Commands;

namespace TBC.PRD.Records.Application.Persons.Commands.UpdatePerson
{
    public class UpdatePersonCommand : ICommand
    {
        public Guid PersonId { get; set; }
        public UpdatePersonModel Model { get; set; }
    }
}
