﻿
using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TBC.PRD.Common.Application.Common.Abstraction;
using TBC.PRD.Common.Application.Common.Handlers;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Common.Abstraction.Commands;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Domain.Exceptions;
using TBC.PRD.Common.Domain.ValueObjects;

namespace TBC.PRD.Records.Application.Persons.Commands.UpdatePerson
{
    public class UpdatePersonCommandHandler : CommandHandler, ICommandHandler<UpdatePersonCommand>
    {
        public UpdatePersonCommandHandler(IApplicationDbContext context, IEventBus eventBus) : base(context, eventBus) { }

        public async Task<Unit> Handle(UpdatePersonCommand request, CancellationToken cancellationToken)
        {
            var person = _context.Set<Person>().SingleOrDefault(e => e.Id == request.PersonId);

            if (person == null)
            {
                throw new DomainException("Person does not exists");
            }

            person = _context.Set<Person>().SingleOrDefault(e => e.PersonalNumber == request.Model.PersonalNumber && e.Id != request.PersonId);

            if (person != null)
            {
                throw new DomainException("Person exists");
            }

            person = _context.Set<Person>().SingleOrDefault(e => e.PhoneNumber == request.Model.PhoneNumber && e.Id != request.PersonId);

            if (person != null)
            {
                throw new DomainException("Phone number exists");
            }



            var city = _context.Set<City>().SingleOrDefault(e => e.Id == request.Model.CityId);

            if (city == null)
            {
                throw new DomainException("City does not exists");
            }

            person = _context.Set<Person>().SingleOrDefault(e => e.Id == request.PersonId);

            person.Update(
                request.Model.FirstName,
                request.Model.LastName,
                request.Model.GenderType,
                PersonalNumber.For(request.Model.PersonalNumber),
                request.Model.BirthDate,
                PhoneNumber.For(request.Model.PhoneNumber),
                request.Model.ThumbnailUrl,
                request.Model.CityId
            );

            await SaveAndPublish(person, cancellationToken);

            return Unit.Value;
        }
    }
}
