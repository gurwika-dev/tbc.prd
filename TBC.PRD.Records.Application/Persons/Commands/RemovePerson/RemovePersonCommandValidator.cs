﻿using FluentValidation;

namespace TBC.PRD.Records.Application.Persons.Commands.RemovePerson
{
    public class RemovePersonCommandValidator : AbstractValidator<RemovePersonCommand>
    {
        public RemovePersonCommandValidator()
        {
            RuleFor(x => x.PersonId).NotNull();
        }
    }
}
