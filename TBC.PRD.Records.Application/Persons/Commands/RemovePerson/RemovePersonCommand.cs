﻿using System;
using TBC.PRD.Common.Domain.Common.Abstraction.Commands;

namespace TBC.PRD.Records.Application.Persons.Commands.RemovePerson
{
    public class RemovePersonCommand : ICommand
    {
        public Guid PersonId { get; set; }
    }
}
