﻿using MediatR;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TBC.PRD.Common.Application.Common.Abstraction;
using TBC.PRD.Common.Application.Common.Handlers;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Common.Abstraction.Commands;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Domain.Exceptions;

namespace TBC.PRD.Records.Application.Persons.Commands.RemovePerson
{
    public class RemovePersonCommandHandler : CommandHandler, ICommandHandler<RemovePersonCommand>
    {
        public RemovePersonCommandHandler(IApplicationDbContext context, IEventBus eventBus) : base(context, eventBus) { }

        public async Task<Unit> Handle(RemovePersonCommand request, CancellationToken cancellationToken)
        {
            var person = _context.Set<Person>().SingleOrDefault(e => e.Id == request.PersonId);

            if (person == null)
            {
                throw new DomainException("Person does not exists");
            }

            person.Delete();

            await SaveAndPublish(person, cancellationToken);

            return Unit.Value;
        }
    }
}
