﻿using TBC.PRD.Common.Domain.Common.Abstraction.Commands;

namespace TBC.PRD.Records.Application.System.Commands.SeedSampleData
{
    public class SeedSampleDataCommand : ICommand { }
}
