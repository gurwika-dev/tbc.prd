﻿using MediatR;
using TBC.PRD.Common.Application.Common.Abstraction;
using TBC.PRD.Common.Domain.Common.Abstraction.Commands;
using System;
using System.Threading;
using System.Threading.Tasks;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Infrastructure.Extentions;
using System.Collections.Generic;
using TBC.PRD.Common.Application.Common.Handlers;

namespace TBC.PRD.Records.Application.System.Commands.SeedSampleData
{
    public class SeedSampleDataCommandHandler: CommandHandler, ICommandHandler<SeedSampleDataCommand>
    {
        public SeedSampleDataCommandHandler(IApplicationDbContext context, IEventBus eventBus) : base(context, eventBus) { }

        public async Task<Unit> Handle(SeedSampleDataCommand request, CancellationToken cancellationToken)
        {
            var cities = new List<string> { "თბილისი",
                                            "ბათუმი",
                                            "ქუთაისი",
                                            "ბაკურიანი",
                                            "გუდაური",
                                            "აბასთუმანი",
                                            "აბაშა",
                                            "აგარა",
                                            "ადიგენი",
                                            "ამბროლაური",
                                            "ანაკლია",
                                            "ანანური",
                                            "არაგვისპირი",
                                            "ასპინძა",
                                            "ახალგორი",
                                            "ახალდაბა",
                                            "ახალქალაქი",
                                            "ახალციხე",
                                            "ახმეტა",
                                            "ბაზალეთი",
                                            "ბაღდათი",
                                            "ბახმარო",
                                            "ბოლნისი",
                                            "ბორჯომი",
                                            "ბულაჩაური",
                                            "გაგრა",
                                            "გალი",
                                            "გარდაბანი",
                                            "გონიო",
                                            "გორი",
                                            "გრიგოლეთი",
                                            "გუდაუთა",
                                            "გულრიფში",
                                            "გურჯაანი",
                                            "დედოფლისწყარო",
                                            "დმანისი",
                                            "დუშეთი",
                                            "ვაზისუბანი",
                                            "ვანი",
                                            "ზედაზენი",
                                            "ზესტაფონი",
                                            "ზუგდიდი",
                                            "თეთრიწყარო",
                                            "თელავი",
                                            "თერჯოლა",
                                            "თიანეთი",
                                            "იგოეთი",
                                            "კარდენახი",
                                            "კასპი",
                                            "კვარიათი",
                                            "კოდა",
                                            "კოდორი",
                                            "ლაგოდეხი",
                                            "ლანჩხუთი",
                                            "ლენტეხი",
                                            "ლიკანი",
                                            "მამკოდა",
                                            "მანგლისი",
                                            "მარნეული",
                                            "მარტვილი",
                                            "მარტყოფი",
                                            "მახინჯაური",
                                            "მესტია",
                                            "მისაქციელი",
                                            "მუხათწყარო",
                                            "მუხიანის აგარაკები",
                                            "მუხრანი",
                                            "მცხეთა",
                                            "ნატანები",
                                            "ნატახტარი",
                                            "ნაფეტვრები",
                                            "ნინოწმინდა",
                                            "ნუნისი",
                                            "ოზურგეთი",
                                            "ონი",
                                            "ოჩამჩირე",
                                            "პანკისის ხეობა",
                                            "ჟინვალი",
                                            "რუსთავი",
                                            "საგარეჯო",
                                            "საგურამო",
                                            "საირმე",
                                            "სამტრედია",
                                            "სართიჭალა",
                                            "სარფი",
                                            "საჩხერე",
                                            "სენაკი",
                                            "სიონი",
                                            "სიღნაღი",
                                            "სურამი",
                                            "სუფსა",
                                            "ტყვარჩელი",
                                            "ტყიბული",
                                            "ურეკი",
                                            "ფასანაური",
                                            "ფოთი",
                                            "ქარელი",
                                            "ქედა",
                                            "ქვიშხეთი"};

            cities.ForEach(displayName => {
                var city = new City(
                    Guid.NewGuid(),
                    displayName
                );

                _context.Set<City>().AddIfNotExists(city, x => x.DisplayName == city.DisplayName);
            });

            await SaveChanges(cancellationToken);

            return Unit.Value;
        }
    }
}
