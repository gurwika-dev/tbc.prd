﻿using FluentValidation;

namespace TBC.PRD.Records.Application.PersonRelations.Queries.GetPersonRelationsList
{
    public class GetPersonRelationsListQueryValidator : AbstractValidator<GetPersonRelationsListQuery>
    {
        public GetPersonRelationsListQueryValidator()
        {
            RuleFor(x => x.PersonId).NotNull();
            RuleFor(x => x.Model.Page).GreaterThanOrEqualTo(0).NotNull();
            RuleFor(x => x.Model.Limit).GreaterThan(1).NotNull();
            RuleFor(x => x.Model.KeyWord).MaximumLength(20);
        }
    }
}
