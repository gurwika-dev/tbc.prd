﻿using System;
using TBC.PRD.Common.Domain.Common.Abstraction.Queries;

namespace TBC.PRD.Records.Application.PersonRelations.Queries.GetPersonRelationsList
{
    public class GetPersonRelationsListQuery : IQuery<GetPersonRelationsListVM>
    {
        public Guid PersonId { get; set; }

        public GetPersonRelationsListModel Model { get; set; }
    }
}
