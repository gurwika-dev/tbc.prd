﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Common.Abstraction.Queries;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Domain.Exceptions;
using TBC.PRD.Records.Application.Common.Types;

namespace TBC.PRD.Records.Application.PersonRelations.Queries.GetPersonRelationsList
{
    public class GetPersonRelationsListQueryHandler : IQueryHandler<GetPersonRelationsListQuery, GetPersonRelationsListVM>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetPersonRelationsListQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GetPersonRelationsListVM> Handle(GetPersonRelationsListQuery request, CancellationToken cancellationToken)
        {
            var person = _context.Set<Person>().SingleOrDefault(e => e.Id == request.PersonId);

            if (person == null)
            {
                throw new DomainException("person does not exists");
            }

            var directEntriesQuery = _context.Set<PersonRelation>()
                                    .Where(e => e.PersonId == request.PersonId)
                                    .Include(e => e.RelatedPerson);

            var indirectEntriesQuery = _context.Set<PersonRelation>()
                                    .Where(e => e.RelatedPersonId == request.PersonId)
                                    .Include(e => e.Person);

            if (!string.IsNullOrEmpty(request.Model.KeyWord))
            {
                directEntriesQuery.Where(e => e.RelatedPerson.FirstName.Contains(request.Model.KeyWord) || 
                                                e.RelatedPerson.LastName.Contains(request.Model.KeyWord) ||
                                                e.RelatedPerson.PersonalNumber.ToString().Contains(request.Model.KeyWord)
                                        );
                indirectEntriesQuery.Where(e => e.Person.FirstName.Contains(request.Model.KeyWord) ||
                                                e.Person.LastName.Contains(request.Model.KeyWord) ||
                                                e.Person.PersonalNumber.ToString().Contains(request.Model.KeyWord)
                                        );
            }

            var directEntriesQuerySelect = directEntriesQuery
                                            .Select(e => new GetPersonRelationsListDTO
                                            {
                                                Id = e.Id,
                                                Type = e.Type,
                                                PersonId = e.PersonId,
                                                Category = PersonRelationCategory.DIRECT,
                                                RelatedPerson = new GetPersonRelationsListRelatedPersonDTO
                                                {
                                                    Id = e.RelatedPerson.Id,
                                                    FirstName = e.RelatedPerson.FirstName,
                                                    LastName = e.RelatedPerson.LastName,
                                                    ThumbnailUrl = e.RelatedPerson.ThumbnailUrl,
                                                    PersonalNumber = e.RelatedPerson.PersonalNumber
                                                }
                                            });

            var indirectEntriesQuerySelect = indirectEntriesQuery
                                                .Select(e => new GetPersonRelationsListDTO
                                                {
                                                    Id = e.Id,
                                                    Type = e.Type,
                                                    PersonId = e.PersonId,
                                                    Category = PersonRelationCategory.INDIRECT,
                                                    RelatedPerson = new GetPersonRelationsListRelatedPersonDTO
                                                    {
                                                        Id = e.Person.Id,
                                                        FirstName = e.Person.FirstName,
                                                        LastName = e.Person.LastName,
                                                        ThumbnailUrl = e.Person.ThumbnailUrl,
                                                        PersonalNumber = e.Person.PersonalNumber
                                                    }
                                                });

            var entriesQuery = directEntriesQuerySelect.Concat(indirectEntriesQuerySelect);

            var count = entriesQuery.Count();
            var entries = await entriesQuery
                                    .Skip(request.Model.Limit * (request.Model.Page - 1))
                                    .Take(request.Model.Limit)
                                    .ToListAsync(cancellationToken);

            return new GetPersonRelationsListVM
            {
                Entries = entries,
                Count = count
            };
        }
    }
}
