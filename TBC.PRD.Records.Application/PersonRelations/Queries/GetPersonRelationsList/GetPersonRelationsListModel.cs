﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TBC.PRD.Records.Application.PersonRelations.Queries.GetPersonRelationsList
{
    public class GetPersonRelationsListModel
    {
        public int Limit { get; set; }
        public int Page { get; set; }
        public string KeyWord { get; set; }
    }
}
