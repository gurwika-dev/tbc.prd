﻿using System.Collections.Generic;

namespace TBC.PRD.Records.Application.PersonRelations.Queries.GetPersonRelationsList
{
    public class GetPersonRelationsListVM
    {
        public IList<GetPersonRelationsListDTO> Entries { get; set; }
        public int Count { get; set; }
    }
}
