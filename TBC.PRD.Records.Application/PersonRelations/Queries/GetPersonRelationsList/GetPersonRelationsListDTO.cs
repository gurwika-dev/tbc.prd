﻿using AutoMapper;
using System;
using TBC.PRD.Common.Application.Mappings.Abstraction;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Domain.Enumarations.Records;
using TBC.PRD.Records.Application.Common.Types;

namespace TBC.PRD.Records.Application.PersonRelations.Queries.GetPersonRelationsList
{
    public class GetPersonRelationsListRelatedPersonDTO
    {
        public Guid Id { get; set; }
        public string ThumbnailUrl { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PersonalNumber { get; set; }
    }

    public class GetPersonRelationsListDTO : IMapFrom<PersonRelation>
    {
        public Guid Id { get; set; }
        public Guid PersonId { get; set; }
        public RelationType Type { get; set; }
        public PersonRelationCategory Category { get; set; }

        public GetPersonRelationsListRelatedPersonDTO RelatedPerson { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<PersonRelation, GetPersonRelationsListDTO>();
        }
    }
}
