﻿using FluentValidation;
using System;

namespace TBC.PRD.Records.Application.PersonRelations.Commands.CreatePersonsRelation
{
    public class CreatePersonsRelationCommandValidator : AbstractValidator<CreatePersonsRelationCommand>
    {
        public CreatePersonsRelationCommandValidator()
        {
            RuleFor(x => x.Model.Id).NotNull();
            RuleFor(x => x.PersonId).NotNull();
            RuleFor(x => x.Model.RelatedPersonId).NotNull()
                .Must((model, field) => field != model.PersonId);
            RuleFor(x => x.Model.Type).NotNull().IsInEnum();
        }
    }
}
