﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TBC.PRD.Common.Application.Common.Abstraction;
using TBC.PRD.Common.Application.Common.Handlers;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Common.Abstraction.Commands;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Domain.Exceptions;

namespace TBC.PRD.Records.Application.PersonRelations.Commands.CreatePersonsRelation
{
    public class CreatePersonsRelationCommandHandler : CommandHandler, ICommandHandler<CreatePersonsRelationCommand>
    {
        public CreatePersonsRelationCommandHandler(IApplicationDbContext context, IEventBus eventBus) : base(context, eventBus) { }

        public async Task<Unit> Handle(CreatePersonsRelationCommand request, CancellationToken cancellationToken)
        {
            var person = _context.Set<Person>().SingleOrDefault(e => e.Id == request.PersonId);

            if (person == null)
            {
                throw new DomainException("person does not exists");
            }

            var personRelation = new PersonRelation(
                request.Model.Id,
                request.PersonId,
                request.Model.RelatedPersonId,
                request.Model.Type
            );

            _context.Add(personRelation);

            await SaveAndPublish(personRelation, cancellationToken);

            return Unit.Value;
        }
    }
}
