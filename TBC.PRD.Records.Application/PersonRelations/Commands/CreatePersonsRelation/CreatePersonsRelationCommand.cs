﻿using System;
using TBC.PRD.Common.Domain.Common.Abstraction.Commands;

namespace TBC.PRD.Records.Application.PersonRelations.Commands.CreatePersonsRelation
{
    public class CreatePersonsRelationCommand : ICommand
    {
        public Guid PersonId { get; set; }
        public CreatePersonsRelationModel Model { get; set; }
    }
}
