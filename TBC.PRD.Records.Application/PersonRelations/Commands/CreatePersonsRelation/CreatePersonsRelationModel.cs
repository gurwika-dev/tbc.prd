﻿using System;
using TBC.PRD.Common.Domain.Enumarations.Records;

namespace TBC.PRD.Records.Application.PersonRelations.Commands.CreatePersonsRelation
{
    public class CreatePersonsRelationModel
    {
        public Guid Id { get; set; }
        public Guid RelatedPersonId { get; set; }
        public RelationType Type { get; set; }
    }
}
