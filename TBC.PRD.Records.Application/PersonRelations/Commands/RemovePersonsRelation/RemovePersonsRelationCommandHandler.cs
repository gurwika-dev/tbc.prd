﻿using MediatR;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TBC.PRD.Common.Application.Common.Abstraction;
using TBC.PRD.Common.Application.Common.Handlers;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Common.Abstraction.Commands;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Domain.Exceptions;

namespace TBC.PRD.Records.Application.PersonRelations.Commands.RemovePersonsRelation
{
    public class RemovePersonsRelationCommandHandler : CommandHandler, ICommandHandler<RemovePersonsRelationCommand>
    {
        public RemovePersonsRelationCommandHandler(IApplicationDbContext context, IEventBus eventBus) : base(context, eventBus) { }

        public async Task<Unit> Handle(RemovePersonsRelationCommand request, CancellationToken cancellationToken)
        {
            var person = _context.Set<Person>().SingleOrDefault(e => e.Id == request.PersonId);

            if (person == null)
            {
                throw new DomainException("Person does not exists");
            }

            var personRelation = _context.Set<PersonRelation>().SingleOrDefault(e => e.Id == request.PersonRelationId);

            if (personRelation == null)
            {
                throw new DomainException("Person reslation does not exists");
            }

            personRelation.Delete();

            await SaveAndPublish(personRelation, cancellationToken);

            return Unit.Value;
        }
    }
}
