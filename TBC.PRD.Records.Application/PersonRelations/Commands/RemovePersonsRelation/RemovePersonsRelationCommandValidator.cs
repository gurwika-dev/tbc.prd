﻿using FluentValidation;

namespace TBC.PRD.Records.Application.PersonRelations.Commands.RemovePersonsRelation
{
    public class RemovePersonsRelationCommandValidator : AbstractValidator<RemovePersonsRelationCommand>
    {
        public RemovePersonsRelationCommandValidator()
        {
            RuleFor(x => x.PersonId).NotNull();
            RuleFor(x => x.PersonRelationId).NotNull();
        }
    }
}
