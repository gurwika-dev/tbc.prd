﻿using System;
using TBC.PRD.Common.Domain.Common.Abstraction.Commands;

namespace TBC.PRD.Records.Application.PersonRelations.Commands.RemovePersonsRelation
{
    public class RemovePersonsRelationCommand : ICommand
    {
        public Guid PersonId { get; set; }
        public Guid PersonRelationId { get; set; }
    }
}
