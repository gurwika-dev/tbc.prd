﻿using TBC.PRD.Common.Domain.Common.Abstraction.Queries;

namespace TBC.PRD.Records.Application.Cities.Queries.GetCitiesList
{
    public class GetCitiesListQuery : IQuery<GetCitiesListVM> 
    { 
        public GetCitiesListModel Model { get; set; }
    }
}
