﻿using FluentValidation;

namespace TBC.PRD.Records.Application.Cities.Queries.GetCitiesList
{
    public class GetCitiesListQueryValidator : AbstractValidator<GetCitiesListQuery>
    {
        public GetCitiesListQueryValidator()
        {
            RuleFor(x => x.Model.KeyWord).MaximumLength(10);
        }
    }
}
