﻿using System.Collections.Generic;

namespace TBC.PRD.Records.Application.Cities.Queries.GetCitiesList
{
    public class GetCitiesListVM
    {
        public IList<GetCitiesListDTO> Entries { get; set; }
        public int Count { get; set; }
    }
}
