﻿using AutoMapper;
using System;
using TBC.PRD.Common.Application.Mappings.Abstraction;
using TBC.PRD.Common.Domain.Entities.Records;

namespace TBC.PRD.Records.Application.Cities.Queries.GetCitiesList
{
    public class GetCitiesListDTO : IMapFrom<City>
    {
        public Guid Id { get; set; }
        public string DisplayName { get; protected set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<City, GetCitiesListDTO>();
        }
    }
}
