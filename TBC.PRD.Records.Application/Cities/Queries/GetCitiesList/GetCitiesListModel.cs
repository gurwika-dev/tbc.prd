﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TBC.PRD.Records.Application.Cities.Queries.GetCitiesList
{
    public class GetCitiesListModel
    {
        public string KeyWord { get; set; }
    }
}
