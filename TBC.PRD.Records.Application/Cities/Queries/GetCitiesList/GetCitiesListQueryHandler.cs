﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Common.Abstraction.Queries;
using TBC.PRD.Common.Domain.Entities.Records;

namespace TBC.PRD.Records.Application.Cities.Queries.GetCitiesList
{
    public class GetCitiesListQueryHandler : IQueryHandler<GetCitiesListQuery, GetCitiesListVM>
    {
        private readonly IApplicationDbContext _context;
        private readonly IMapper _mapper;

        public GetCitiesListQueryHandler(IApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<GetCitiesListVM> Handle(GetCitiesListQuery request, CancellationToken cancellationToken)
        {
            var entriesQuery = _context.Set<City>();

            if (!string.IsNullOrEmpty(request.Model.KeyWord))
            {
                entriesQuery.Where(e => e.DisplayName.Contains(request.Model.KeyWord));
            }

            var entries = await entriesQuery
                                    .ProjectTo<GetCitiesListDTO>(_mapper.ConfigurationProvider)
                                    .ToListAsync(cancellationToken);

            return new GetCitiesListVM
            {
                Entries = entries,
                Count = entries.Count
            };
        }
    }
}
