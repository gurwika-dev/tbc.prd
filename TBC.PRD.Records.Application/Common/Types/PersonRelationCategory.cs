﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TBC.PRD.Records.Application.Common.Types
{
    public enum PersonRelationCategory
    {
        DIRECT,
        INDIRECT
    }
}
