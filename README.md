# TBC.PRD

TBC Personal Record database platform

## Installation

Use the container manager [Docker](https://www.docker.com/) to run.

## Usage

```bash
docker-compose build --no-cache
docker-compose up -d --force-recreate
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
