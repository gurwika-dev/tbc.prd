﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TBC.PRD.Common.Application.Common.Abstraction;
using TBC.PRD.Common.Infrastructure.Common;
using TBC.PRD.Common.Infrastructure.Services;

namespace TBC.PRD.Common.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddCommonInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IEventBus, EventBus>();

            services.AddScoped<IUserManagerService, UserManagerService>();
            services.AddScoped<IUserRoleManagerService, UserRoleManagerService>();
            services.AddTransient<ICurrentUserService, CurrentUserService>();
            services.AddTransient<IDateTimeService, DateTimeService>();

            return services;
        }
    }
}
