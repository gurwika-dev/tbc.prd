﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;
using NSwag;

namespace TBC.PRD.Common.Infrastructure.Extentions
{
    public static class AddSwaggerDocumentExtensions
    {
        public static IServiceCollection AddSwaggerDocument(this IServiceCollection services)
        {
            return services.AddOpenApiDocument(options =>
            {
                options.DocumentName = "Api documentation";
                options.Title = "TBC.PRD api";
                options.Description = "This api serves to provide documentation";
                options.GenerateEnumMappingDescription = true;


                var accessTokenSecurityScheme = new OpenApiSecurityScheme();
                accessTokenSecurityScheme.AuthorizationUrl = "https://localhost:44381/";
                accessTokenSecurityScheme.Flow = OpenApiOAuth2Flow.Password;
                accessTokenSecurityScheme.Scheme = JwtBearerDefaults.AuthenticationScheme;
                accessTokenSecurityScheme.Type = OpenApiSecuritySchemeType.ApiKey;
                accessTokenSecurityScheme.In = OpenApiSecurityApiKeyLocation.Header;
                accessTokenSecurityScheme.Name = "Authorization";
                accessTokenSecurityScheme.Description = "Copy 'Bearer ' + valid JWT token into field";

                options.AddSecurity("my-app bearer token", new[] { "TBC.PRD.Identity.LauncherAPI", "TBC.PRD.Records.LauncherAPI" },
                    accessTokenSecurityScheme);
            });
        }
    }
}
