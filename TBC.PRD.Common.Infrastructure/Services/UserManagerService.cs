﻿using IdentityModel;
using IdentityServer4;
using Microsoft.AspNetCore.Identity;
using TBC.PRD.Common.Application.Common.Abstraction;
using TBC.PRD.Common.Application.Common.Models;
using TBC.PRD.Common.Application.Exceptions;
using TBC.PRD.Common.Domain.Entities.Application;
using TBC.PRD.Common.Domain.Enumarations.Application;
using TBC.PRD.Common.Infrastructure.Extentions;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace TBC.PRD.Common.Infrastructure.Services
{
    public class UserManagerService : IUserManagerService
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public UserManagerService(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public async Task<(Result Result, string UserId)> CreateUserAsync(string userName, string password)
        {
            var user = new ApplicationUser
            {
                UserName = userName,
                Email = userName,
            };

            var result = await _userManager.CreateAsync(user, password);

            return (result.ToApplicationResult(), user.Id);
        }

        public async Task<(Result Result, string UserId)> CreateUserAsync(ApplicationUser user, ApplicationUserType type, string password)
        {
            var result = await _userManager.CreateAsync(user, password);

            if (result.Succeeded)
            {
                await _userManager.AddClaimAsync(user, new Claim(JwtClaimTypes.Name, user.UserName));
                await _userManager.AddClaimAsync(user, new Claim(IdentityServerConstants.StandardScopes.Email, user.Email));
                await _userManager.AddClaimAsync(user, new Claim(ClaimTypes.Role, type.ToString()));

                result = await _userManager.AddToRoleAsync(user, type.ToString());
            }

            return (result.ToApplicationResult(), user.Id);
        }

        public async Task<Result> DeleteUserAsync(string userId)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.Id == userId);

            if(user == null)
            {
                throw new NotFoundException(userId, typeof(ApplicationUser).Name);
            }

            var result = await _userManager.DeleteAsync(user);

            return result.ToApplicationResult();
        }

        public async Task<bool> UserExistsAsync(string userName)
        {
            return await Task.FromResult<bool>(_userManager.Users.Any(u => u.UserName == userName));
        }
    }
}
