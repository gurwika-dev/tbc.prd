﻿using AutoMapper;

namespace TBC.PRD.Common.Application.Mappings.Abstraction
{
    public interface IMapFrom<T>
    {
        void Mapping(Profile profile) => profile.CreateMap(typeof(T), GetType());
    }
}
