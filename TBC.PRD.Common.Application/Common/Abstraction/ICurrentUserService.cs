﻿using System.Net;

namespace TBC.PRD.Common.Application.Common.Abstraction
{
    public interface ICurrentUserService
    {
        IPAddress Ip { get; }
        string UserId { get; }
        bool IsAuthenticated { get; }
    }
}
