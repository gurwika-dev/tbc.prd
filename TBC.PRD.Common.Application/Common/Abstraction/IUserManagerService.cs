﻿using TBC.PRD.Common.Application.Common.Models;
using TBC.PRD.Common.Domain.Entities.Application;
using TBC.PRD.Common.Domain.Enumarations.Application;
using System.Threading.Tasks;

namespace TBC.PRD.Common.Application.Common.Abstraction
{
    public interface IUserManagerService
    {
        Task<bool> UserExistsAsync(string userName);
        Task<(Result Result, string UserId)> CreateUserAsync(ApplicationUser user, ApplicationUserType type, string password);
        Task<Result> DeleteUserAsync(string userId);
    }
}
