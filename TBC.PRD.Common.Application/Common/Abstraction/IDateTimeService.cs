﻿using System;

namespace TBC.PRD.Common.Application.Common.Abstraction
{
    public interface IDateTimeService
    {
        DateTime Now { get; }
    }
}
