﻿using TBC.PRD.Common.Application.Common.Abstraction;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Common.Concrete.Aggregates;
using System.Threading;
using System.Threading.Tasks;

namespace TBC.PRD.Common.Application.Common.Handlers
{
    public abstract class CommandHandler
    {
        protected readonly IEventBus _eventBus;
        protected readonly IApplicationDbContext _context;

        public CommandHandler(IApplicationDbContext context, IEventBus eventBus)
        {
            _context = context;
            _eventBus = eventBus;
        }

        protected async Task SaveAndPublish(EventSourcedAggregate eventSourcedAggregate, CancellationToken cancellationToken = default(CancellationToken))
        {
            await SaveChanges();

            await _eventBus.Publish(eventSourcedAggregate.PendingEvents.ToArray());
        }

        protected async Task SaveChanges(CancellationToken cancellationToken = default(CancellationToken))
        {
            await _context.SaveChangesAsync(cancellationToken);
        }
    }
}
