﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using IdentityServer4.Models;
using TBC.PRD.Common.Domain.Entities.Application;
using TBC.PRD.Common.Persistence.Context;
using TBC.PRD.Common.Persistence;
using Microsoft.AspNetCore.Hosting;

namespace TBC.PRD.Identity.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddIdentityInfrastructure(this IServiceCollection services, IConfiguration configuration, IWebHostEnvironment environment)
        {
            services.AddIdentityServer(options =>
            {
                options.IssuerUri = "https://myidentity.com";
            })
              .AddApiAuthorization<ApplicationUser, ApplicationDbContext>(options =>
              {
                  options.ApiResources.Add(new ApiResource("TBC.PRD.Records.LauncherAPI", "Records.Api"));

                  options.Clients.Add(new Client
                  {
                      ClientId = "TBC.PRD.Api",
                      ClientSecrets = { new Secret("API.Secret".Sha256()) },
                      AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,
                      AllowedScopes = { "TBC.PRD.Identity.LauncherAPI", "TBC.PRD.Records.LauncherAPI" }
                  });

                  options.Clients.Add(new Client
                  {
                      ClientId = "angular_spa",
                      ClientName = "Angular SPA",
                      AllowedGrantTypes = GrantTypes.Implicit,
                      AllowedScopes = { "TBC.PRD.Identity.LauncherAPI", "TBC.PRD.Records.LauncherAPI" },
                      RedirectUris = { "http://localhost:4200/auth-callback" },
                      PostLogoutRedirectUris = { "http://localhost:4200/" },
                      AllowedCorsOrigins = { "http://localhost:4200" },
                      AllowAccessTokensViaBrowser = true,
                      AccessTokenLifetime = 3600
                  });
              })
            .AddSigningCredential(configuration.GetSection("SigninKeyCredentials"), environment, @"\TBC.PRD.Identity.Launcher");

            return services;
        }
    }
}
