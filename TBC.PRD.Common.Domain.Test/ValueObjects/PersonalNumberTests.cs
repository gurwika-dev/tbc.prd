﻿using TBC.PRD.Common.Domain.Exceptions;
using TBC.PRD.Common.Domain.ValueObjects;
using Xunit;

namespace MRKT.Common.Domain.Test.ValueObjects
{
    public class PersonalNumberTests
    {
        [Fact]
        public void ExplicitConversionFromStringToEmail()
        {
            var personalNumberString = "12345678910";
            var personalNumber = (PersonalNumber)personalNumberString;

            Assert.Equal(personalNumberString, personalNumber.ToString());
        }

        [Fact]
        public void ToStringShouldReturnCorrectEmail()
        {
            var personalNumberString = "12345678910";
            var personalNumber = PersonalNumber.For(personalNumberString);

            Assert.Equal(personalNumberString, personalNumber.ToString());
        }

        [Fact]
        public void ShouldThrow()
        {
            Assert.Throws<InvalidPersonalNumberException>(() => (PersonalNumber)"123456789AB");
        }
    }
}
