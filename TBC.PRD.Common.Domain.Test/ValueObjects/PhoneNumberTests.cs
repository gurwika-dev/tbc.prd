﻿using TBC.PRD.Common.Domain.Enumarations.ValueObjects;
using TBC.PRD.Common.Domain.Exceptions;
using TBC.PRD.Common.Domain.ValueObjects;
using Xunit;

namespace MRKT.Common.Domain.Test.ValueObjects
{
    public class PhoneNumberTests
    {
        [Fact]
        public void ShouldHaveCorrectSuffixAndNumber()
        {
            var phoneNumber = PhoneNumber.For("0-577272727");

            Assert.Equal(PhoneNumberType.MOBILE, phoneNumber.Type);
            Assert.Equal("577272727", phoneNumber.Number);
        }

        [Fact]
        public void ExplicitConversionFromStringToPhoneNumber()
        {
            var phoneNumberString = "0-577272727";
            var phoneNumber = (PhoneNumber)phoneNumberString;
              
            Assert.Equal(PhoneNumberType.MOBILE, phoneNumber.Type);
            Assert.Equal("577272727", phoneNumber.Number);
        }

        [Fact]
        public void ToStringShouldReturnCorrectNumber()
        {
            var phoneNumberString = "0-577272727";
            var phoneNumber = PhoneNumber.For(phoneNumberString);

            Assert.Equal(phoneNumberString, phoneNumber.ToString());
        }

        [Fact]
        public void ShouldInvalidPhoneNumberExceptionForInvalidPhoneNumber()
        {
            Assert.Throws<InvalidPhoneNumberException>(() => (PhoneNumber)"5-577272727");
        }
    }
}
