﻿using System;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Domain.Entities.Records.Events;
using TBC.PRD.Common.Domain.Enumarations.Records;
using Xunit;

namespace TBC.PRD.Common.Domain.Test.Entities
{
    public class PersonRelationsTests
    {
        [Fact]
        public void ShouldMatchAllCreatedFileds()
        {
            var id = Guid.NewGuid();
            var personId = Guid.NewGuid();
            var relatedPersonId = Guid.NewGuid();
            var type = RelationType.ACQUAINTANCE;

            var personRelation = new PersonRelation(
                id,
                personId,
                relatedPersonId,
                type
            );

            Assert.Equal(personId, personRelation.PersonId);
            Assert.Equal(relatedPersonId, personRelation.RelatedPersonId);
            Assert.IsType<PersonRelationCreatedEvent>(personRelation.PendingEvents.Peek());
        }


        [Fact]
        public void ShouldDelete()
        {
            var id = Guid.NewGuid();
            var personId = Guid.NewGuid();
            var relatedPersonId = Guid.NewGuid();
            var type = RelationType.ACQUAINTANCE;

            var personRelation = new PersonRelation(
                id,
                personId,
                relatedPersonId,
                type
            );

            personRelation.Delete();

            Assert.NotNull(personRelation.DeletedAt);
            Assert.Equal(2, personRelation.PendingEvents.Count);
            Assert.IsType<PersonRelationDeletedEvent>(personRelation.PendingEvents.ToArray()[1]);
        }
    }
}
