﻿using System;
using TBC.PRD.Common.Domain.Entities.Records;
using Xunit;

namespace MRKT.Common.Domain.Test.Entities
{
    public class CityTests
    {
        [Fact]
        public void ShouldMatchAllCreatedFileds()
        {
            var id = Guid.NewGuid();
            var displayName = "City name";

            var city = new City(
                id,
                displayName
            );

            Assert.Equal(id, city.Id);
            Assert.Equal(displayName, city.DisplayName);
        }

        [Fact]
        public void ShouldMatchAllUpdatedFileds()
        {
            var id = Guid.NewGuid();
            var displayName = "City name";

            var city = new City(
                id,
                displayName
            );

            displayName = "City name : updated";
            city.Update(displayName);

            Assert.Equal(displayName, city.DisplayName);
        }

        [Fact]
        public void ShouldDelete()
        {
            var id = Guid.NewGuid();
            var displayName = "City name";

            var city = new City(
                id,
                displayName
            );

            city.Delete();

            Assert.NotNull(city.DeletedAt);
        }
    }
}
