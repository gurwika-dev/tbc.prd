﻿using System;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Domain.Entities.Records.Events;
using TBC.PRD.Common.Domain.Enumarations.Application;
using TBC.PRD.Common.Domain.ValueObjects;
using Xunit;

namespace MRKT.Common.Domain.Test.Entities
{
    public class PersonTests
    {
        [Fact]
        public void ShouldMatchAllCreatedFileds()
        {
            var id = Guid.NewGuid();
            var firstName = "Test";
            var lastName = "User";
            var genderType = GenderType.FEMALE;
            var personalNumber = PersonalNumber.For("12345678910");
            var birthDate = DateTime.Now;
            var phoneNumber = PhoneNumber.For("0-577272727");
            var thumbnailUrl = "https://example.com/thumbnail.png";
            var cityId = Guid.NewGuid();

            var person = new Person(
                id,
                firstName,
                lastName,
                genderType,
                personalNumber,
                birthDate,
                phoneNumber,
                thumbnailUrl,
                cityId
            );

            Assert.Equal(id, person.Id);
            Assert.Equal(firstName, person.FirstName);
            Assert.Equal(lastName, person.LastName);
            Assert.Equal(genderType, person.GenderType);
            Assert.Equal(personalNumber, person.PersonalNumber);
            Assert.Equal(birthDate, person.BirthDate);
            Assert.Equal(phoneNumber, person.PhoneNumber);
            Assert.Equal(thumbnailUrl, person.ThumbnailUrl);
            Assert.Equal(cityId, person.CityId);
            Assert.IsType<PersonCreatedEvent>(person.PendingEvents.Peek());
        }

        [Fact]
        public void ShouldMatchAllUpdatedFileds()
        {
            var id = Guid.NewGuid();
            var firstName = "Test";
            var lastName = "User";
            var genderType = GenderType.FEMALE;
            var personalNumber = PersonalNumber.For("12345678910");
            var birthDate = DateTime.Now;
            var phoneNumber = PhoneNumber.For("0-577272727");
            var thumbnailUrl = "https://example.com/thumbnail.png";
            var cityId = Guid.NewGuid();

            var person = new Person(
                id,
                firstName,
                lastName,
                genderType,
                personalNumber,
                birthDate,
                phoneNumber,
                thumbnailUrl,
                cityId
            );

            firstName = "Test : updated";
            lastName = "User : updated";
            birthDate = DateTime.Now.AddDays(-17);
            genderType = GenderType.MALE;
            phoneNumber = PhoneNumber.For("2-577272727");
            thumbnailUrl = "https://example.com/thumbnail";
            cityId = Guid.NewGuid();

            person.Update(firstName, lastName, genderType, personalNumber, birthDate, phoneNumber, thumbnailUrl, cityId);

            Assert.Equal(id, person.Id);
            Assert.Equal(firstName, person.FirstName);
            Assert.Equal(lastName, person.LastName);
            Assert.Equal(genderType, person.GenderType);
            Assert.Equal(personalNumber, person.PersonalNumber);
            Assert.Equal(birthDate, person.BirthDate);
            Assert.Equal(phoneNumber, person.PhoneNumber);
            Assert.Equal(thumbnailUrl, person.ThumbnailUrl);
            Assert.Equal(cityId, person.CityId);
            Assert.Equal(2, person.PendingEvents.Count);
            Assert.IsType<PersonUpdatedEvent>(person.PendingEvents.ToArray()[1]);
        }

        [Fact]
        public void ShouldDelete()
        {
            var id = Guid.NewGuid();
            var firstName = "Test";
            var lastName = "User";
            var genderType = GenderType.FEMALE;
            var personalNumber = PersonalNumber.For("12345678910");
            var birthDate = DateTime.Now;
            var phoneNumber = PhoneNumber.For("0-577272727");
            var thumbnailUrl = "https://example.com/thumbnail.png";
            var cityId = Guid.NewGuid();

            var person = new Person(
                id,
                firstName,
                lastName,
                genderType,
                personalNumber,
                birthDate,
                phoneNumber,
                thumbnailUrl,
                cityId
            );

            person.Delete();

            Assert.NotNull(person.DeletedAt);
            Assert.Equal(2, person.PendingEvents.Count);
            Assert.IsType<PersonDeletedEvent>(person.PendingEvents.ToArray()[1]);
        }
    }
}
