﻿using TBC.PRD.Common.Domain.Common.Abstraction.Commands;

namespace TBC.PRD.Identity.Application.System.Commands.SeedSampleData
{
    public class SeedSampleDataCommand : ICommand { }
}
