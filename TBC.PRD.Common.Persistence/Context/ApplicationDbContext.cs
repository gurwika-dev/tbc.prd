﻿using IdentityServer4.EntityFramework.Options;
using Microsoft.AspNetCore.ApiAuthorization.IdentityServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using TBC.PRD.Common.Application.Common.Abstraction;
using TBC.PRD.Common.Application.Context.Abstraction;
using TBC.PRD.Common.Domain.Common.Concrete.Aggregates;
using TBC.PRD.Common.Domain.Entities.Application;
using TBC.PRD.Common.Infrastructure.Extenctions;
using System.Threading;
using System.Threading.Tasks;
using TBC.PRD.Common.Domain.Common.Abstraction.Aggregates;

namespace TBC.PRD.Common.Persistence.Context
{
    public class ApplicationDbContext : ApiAuthorizationDbContext<ApplicationUser>, IApplicationDbContext
    {
        private readonly ICurrentUserService _currentUserService;
        private readonly IDateTimeService _dateTimeService;

        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options,
            IOptions<OperationalStoreOptions> operationalStoreOptions) : base(options, operationalStoreOptions)
        {
        }

        public ApplicationDbContext(
            DbContextOptions<ApplicationDbContext> options,
            IOptions<OperationalStoreOptions> operationalStoreOptions,
            ICurrentUserService currentUserService,
            IDateTimeService dateTimeService) : base(options, operationalStoreOptions)
        {
            _currentUserService = currentUserService;
            _dateTimeService = dateTimeService;
        }

        public override Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            foreach (var entry in ChangeTracker.Entries<Aggregate>())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.Entity.UpdateAddedCredentials(_dateTimeService.Now, _currentUserService.UserId);
                        break;
                    case EntityState.Modified:
                        entry.Entity.UpdateModifiedCredentials(_dateTimeService.Now, _currentUserService.UserId);
                        break;
                }
            }

            return base.SaveChangesAsync(cancellationToken);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyAllConfigurations();
            modelBuilder.ApplyGlobalFilters<IAggregate>(e => e.DeletedAt == null);
        }
    }
}
