﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TBC.PRD.Common.Domain.Entities.Records;

namespace TBC.PRD.Common.Persistence.Configuration.Identity
{
    public class CityConfiguration : IEntityTypeConfiguration<City>
    {
        public void Configure(EntityTypeBuilder<City> builder)
        {
            builder.Property(e => e.DisplayName)
                .IsRequired()
                .HasMaxLength(200);
        }
    }
}