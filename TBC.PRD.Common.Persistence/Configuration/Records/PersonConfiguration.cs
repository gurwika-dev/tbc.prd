﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TBC.PRD.Common.Domain.Entities.Records;
using TBC.PRD.Common.Domain.ValueObjects;

namespace TBC.PRD.Common.Persistence.Configuration.Identity
{
    public class PersonConfiguration : IEntityTypeConfiguration<Person>
    {
        public void Configure(EntityTypeBuilder<Person> builder)
        {
            builder.Ignore(c => c.PendingEvents);

            builder.Property(e => e.FirstName)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(e => e.LastName)
                .IsRequired()
                .HasMaxLength(200);

            builder.Property(p => p.PersonalNumber)
                    .HasConversion(
                        personalNumber => personalNumber.ToString(),
                        dbValue => PersonalNumber.For(dbValue)
                    )
                .HasMaxLength(11);

            builder.Property(p => p.PhoneNumber)
                    .HasConversion(
                        phoneNumber => phoneNumber.ToString(),
                        dbValue => PhoneNumber.For(dbValue)
                    )
                .HasMaxLength(52);

            builder.Property(e => e.ThumbnailUrl)
                .IsRequired()
                .HasMaxLength(200);

            builder.HasOne(e => e.City)
                .WithMany(e => e.Persons)
                .HasForeignKey(e => e.CityId);
        }
    }
}