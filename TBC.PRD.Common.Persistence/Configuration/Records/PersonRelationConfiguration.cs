﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TBC.PRD.Common.Domain.Entities.Records;

namespace TBC.PRD.Common.Persistence.Configuration.Identity
{
    public class PersonRelationConfiguration : IEntityTypeConfiguration<PersonRelation>
    {
        public void Configure(EntityTypeBuilder<PersonRelation> builder)
        {
            builder.Ignore(c => c.PendingEvents);

            builder.HasOne(e => e.Person)
                .WithMany(e => e.Persons)
                .HasForeignKey(e => e.PersonId);

            builder.HasOne(e => e.RelatedPerson)
                .WithMany(e => e.RelatedPersons)
                .HasForeignKey(e => e.RelatedPersonId);
        }
    }
}
