﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TBC.PRD.Common.Persistence.Migrations
{
    public partial class RenameType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RelationType",
                table: "PersonRelation");

            migrationBuilder.AddColumn<int>(
                name: "Type",
                table: "PersonRelation",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "PersonRelation");

            migrationBuilder.AddColumn<int>(
                name: "RelationType",
                table: "PersonRelation",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }
    }
}
