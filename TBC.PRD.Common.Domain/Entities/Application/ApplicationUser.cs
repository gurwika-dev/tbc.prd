﻿
using Microsoft.AspNetCore.Identity;

namespace TBC.PRD.Common.Domain.Entities.Application
{
    public class ApplicationUser: IdentityUser 
    {
        public ApplicationUser()
        {
        }

        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }

        public ApplicationUser(string email, string firstName, string lastName)
        {
            Email = email;
            UserName = email;
            FirstName = firstName;
            LastName = lastName;
        }
    
    }
}
