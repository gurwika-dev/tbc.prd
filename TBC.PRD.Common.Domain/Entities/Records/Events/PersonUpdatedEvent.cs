﻿using TBC.PRD.Common.Domain.Common.Abstraction.Events;
using System;

namespace TBC.PRD.Common.Domain.Entities.Records.Events
{
    public class PersonUpdatedEvent : IEvent
    {
        public Guid Id { get; protected set; }
        public string Payload { get; }

        public PersonUpdatedEvent(
            Guid id,
            string payload = null
        )
        {
            Id = id;
            Payload = payload;
        }
    }
}
