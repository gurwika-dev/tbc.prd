﻿using System;
using System.Collections.Generic;
using System.Text;
using TBC.PRD.Common.Domain.Common.Abstraction.Events;

namespace TBC.PRD.Common.Domain.Entities.Records.Events
{
    public class PersonRelationDeletedEvent : IEvent
    {
        public Guid Id { get; protected set; }

        public string Payload { get; protected set; }

        public PersonRelationDeletedEvent(Guid id)
        {
            Id = id;
        }
    }
}
