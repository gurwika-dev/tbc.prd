﻿using System;
using System.Collections.Generic;
using System.Text;
using TBC.PRD.Common.Domain.Common.Abstraction.Events;

namespace TBC.PRD.Common.Domain.Entities.Records.Events
{
    public class PersonRelationCreatedEvent : IEvent
    {
        public Guid Id { get; protected set; }
        public string Payload { get; protected set; }

        public PersonRelationCreatedEvent(
            Guid id,
            string payload = null
        )
        {
            Id = id;
            Payload = payload;
        }
    }
}
