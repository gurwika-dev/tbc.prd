﻿using TBC.PRD.Common.Domain.Common.Abstraction.Events;
using System;

namespace TBC.PRD.Common.Domain.Entities.Records.Events
{
    public class PersonDeletedEvent : IEvent
    {
        public Guid Id { get; protected set; }

        public string Payload { get; protected set; }

        public PersonDeletedEvent(Guid id)
        {
            Id = id;
        }
    }
}
