﻿using System;
using System.Collections.Generic;
using System.Text;
using TBC.PRD.Common.Domain.Common.Concrete.Aggregates;

namespace TBC.PRD.Common.Domain.Entities.Records
{
    public class City : Aggregate
    {
        public City()
        {
            Persons = new HashSet<Person>();
        }

        public string DisplayName { get; protected set; }

        public virtual ICollection<Person> Persons { get; private set; }

        public City(Guid id, string displayName)
        {
            Id = id;
            DisplayName = displayName;
        }

        public void Update( string displayName)
        {
            DisplayName = displayName;
        }
    }
}
