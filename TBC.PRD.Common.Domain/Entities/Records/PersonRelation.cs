﻿using Newtonsoft.Json;
using System;
using TBC.PRD.Common.Domain.Common.Concrete.Aggregates;
using TBC.PRD.Common.Domain.Entities.Records.Events;
using TBC.PRD.Common.Domain.Enumarations.Records;

namespace TBC.PRD.Common.Domain.Entities.Records
{
    public class PersonRelation : EventSourcedAggregate
    {
        public PersonRelation() { }

        public RelationType Type { get; protected set; }
        public virtual Person Person { get; protected set; }
        public Guid PersonId { get; protected set; }
        public virtual Person RelatedPerson { get; protected set; }
        public Guid RelatedPersonId { get; protected set; }

        public PersonRelation(Guid id, Guid personId, Guid relatedPersonId, RelationType type)
        {
            Id = id;
            PersonId = personId;
            RelatedPersonId = relatedPersonId;
            Type = type;

            RiseEvent(
                new PersonRelationCreatedEvent(
                    Id
                )
            );
        }

        public new void Delete()
        {
            base.Delete();

            RiseEvent(
                new PersonRelationDeletedEvent(
                    Id
                )
            );
        }
    }
}
