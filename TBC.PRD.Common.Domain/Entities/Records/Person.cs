﻿using TBC.PRD.Common.Domain.Enumarations.Application;
using System;
using TBC.PRD.Common.Domain.ValueObjects;
using TBC.PRD.Common.Domain.Common.Concrete.Aggregates;
using System.Collections.Generic;
using Newtonsoft.Json;
using TBC.PRD.Common.Domain.Entities.Records.Events;

namespace TBC.PRD.Common.Domain.Entities.Records
{
    public class Person : EventSourcedAggregate
    {
        public Person()
        {
            Persons = new HashSet<PersonRelation>();
            RelatedPersons = new HashSet<PersonRelation>();
        }

        public string FirstName { get; protected set; }
        public string LastName { get; protected set; }
        public GenderType GenderType { get; protected set; }
        public string PersonalNumber { get; protected set; }
        public DateTime BirthDate { get; protected set; }
        public string PhoneNumber { get; protected set; }
        public string ThumbnailUrl { get; protected set; }
        public virtual City City { get; protected set; }
        public Guid CityId { get; set; }

        public virtual ICollection<PersonRelation> Persons { get; private set; }
        public virtual ICollection<PersonRelation> RelatedPersons { get; private set; }

        public Person(Guid id, string firstName, string lastName, GenderType genderType, PersonalNumber personalNumber, DateTime birthDate, PhoneNumber phoneNumber, string thumbnailUrl, Guid cityId)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            GenderType = genderType;
            PersonalNumber = personalNumber;
            BirthDate = birthDate;
            PhoneNumber = phoneNumber;
            ThumbnailUrl = thumbnailUrl;
            CityId = cityId;

            RiseEvent(
                new PersonCreatedEvent(
                    Id
                )
            );
        }

        public void Update(string firstName, string lastName, GenderType genderType, PersonalNumber personalNumber, DateTime birthDate, PhoneNumber phoneNumber, string thumbnailUrl, Guid cityId)
        {
            FirstName = firstName;
            LastName = lastName;
            GenderType = genderType;
            PersonalNumber = personalNumber;
            BirthDate = birthDate;
            PhoneNumber = phoneNumber;
            ThumbnailUrl = thumbnailUrl;
            CityId = cityId;

            RiseEvent(
                new PersonUpdatedEvent(
                    Id
                )
            );
        }

        public new void Delete()
        {
            base.Delete();

            RiseEvent(
                new PersonDeletedEvent(
                    Id
                )
            );
        }
    }
}
