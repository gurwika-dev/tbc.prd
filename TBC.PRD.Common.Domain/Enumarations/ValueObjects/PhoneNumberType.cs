﻿namespace TBC.PRD.Common.Domain.Enumarations.ValueObjects
{
    public enum PhoneNumberType
    {
        MOBILE,
        OFFICE,
        HOME
    }
}
