﻿namespace TBC.PRD.Common.Domain.Enumarations.Records
{
    public enum RelationType
    {
        COLLEAGUE,
        ACQUAINTANCE,
        RELATIVE,
        OTHER
    }
}
