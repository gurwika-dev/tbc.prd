﻿namespace TBC.PRD.Common.Domain.Enumarations.Application
{
    public enum GenderType
    {
        FEMALE,
        MALE,
    }
}
