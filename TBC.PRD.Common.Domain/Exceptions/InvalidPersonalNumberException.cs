﻿using System;

namespace TBC.PRD.Common.Domain.Exceptions
{
    public class InvalidPersonalNumberException : DomainException
    {
        public InvalidPersonalNumberException(string personalNumber, Exception ex) : base($"Personal \"{personalNumber}\" is invalid.", ex) { }
    }
}
