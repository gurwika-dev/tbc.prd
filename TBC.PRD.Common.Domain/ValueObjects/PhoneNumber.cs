﻿using TBC.PRD.Common.Domain.Common.Concrete.ValueObjects;
using TBC.PRD.Common.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text;
using TBC.PRD.Common.Domain.Enumarations.ValueObjects;

namespace TBC.PRD.Common.Domain.ValueObjects
{
    public class PhoneNumber : ValueObject
    {
        public PhoneNumberType Type { get; private set; }
        public string Number { get; private set; }

        private PhoneNumber() { }

        public static PhoneNumber For(string phoneNumberString)
        {
            var phoneNumber = new PhoneNumber();

            try
            {
                PhoneNumberType type;
                var phoneNumberSegments = phoneNumberString.Split('-');

                if (Enum.TryParse(phoneNumberSegments[0], out type) && PhoneNumberType.IsDefined(typeof(PhoneNumberType), type))
                {
                    phoneNumber.Type = type;
                    phoneNumber.Number = phoneNumberSegments[1];
                } else
                {
                    throw new Exception("Invalid type");
                }
            }
            catch (Exception ex)
            {
                throw new InvalidPhoneNumberException(phoneNumber, ex);
            }

            return phoneNumber;
        }

        public static implicit operator string(PhoneNumber phoneNumber)
        {
            return phoneNumber.ToString();
        }

        public static explicit operator PhoneNumber(string phoneNumber)
        {
            return For(phoneNumber);
        }

        public override string ToString()
        {
            return $"{(int)Type}-{Number}";
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Type;
            yield return Number;
        }
    }
}
