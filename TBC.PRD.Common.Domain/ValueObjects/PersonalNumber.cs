﻿using TBC.PRD.Common.Domain.Common.Concrete.ValueObjects;
using TBC.PRD.Common.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace TBC.PRD.Common.Domain.ValueObjects
{
    public class PersonalNumber : ValueObject
    {
        public string Number { get; private set; }

        private PersonalNumber() { }
        public static PersonalNumber For(string PersonalNumberString)
        {
            var PersonalNumber = new PersonalNumber();

            try
            {
                string pattern = @"^([0-9]{11,11})$";
                Match match = Regex.Match(PersonalNumberString, pattern, RegexOptions.IgnoreCase);

                if (!match.Success)
                {
                    throw new Exception();
                }

                PersonalNumber.Number = PersonalNumberString;
            }
            catch (Exception ex)
            {
                throw new InvalidPersonalNumberException(PersonalNumberString, ex);
            }

            return PersonalNumber;
        }

        public static implicit operator string(PersonalNumber PersonalNumber)
        {
            return PersonalNumber.ToString();
        }

        public static explicit operator PersonalNumber(string PersonalNumber)
        {
            return For(PersonalNumber);
        }

        public override string ToString()
        {
            return $"{Number}";
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Number;
        }
    }
}
