﻿using MediatR;
using System;

namespace TBC.PRD.Common.Domain.Common.Abstraction.Events
{
    public interface IEvent : INotification
    {
        Guid Id { get; }
        public string Payload { get; }
    }
}
