﻿using MediatR;

namespace TBC.PRD.Common.Domain.Common.Abstraction.Commands
{
    public interface ICommand : IRequest { }
}
