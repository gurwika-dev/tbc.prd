﻿using System;

namespace TBC.PRD.Common.Domain.Common.Abstraction.Aggregates
{
    public interface IAggregate
    {
        Guid Id { get; }
        public DateTime CreatedAt { get; }
        public string LastModifiedBy { get; }
        public DateTime? LastModified { get; }
        public DateTime? DeletedAt { get; }
    }
}
