﻿using MediatR;

namespace TBC.PRD.Common.Domain.Common.Abstraction.Queries
{
    public interface IQuery<out TResponse> : IRequest<TResponse>
    {
    }
}
