﻿using MediatR;

namespace TBC.PRD.Common.Domain.Common.Abstraction.Queries
{
    public interface IQueryHandler<in TQuery, TResponse> : IRequestHandler<TQuery, TResponse>
           where TQuery : IQuery<TResponse>
    { 
    }
}
